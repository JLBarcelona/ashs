<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserAccountController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\ProblemController;
use App\Http\Controllers\StudentProfileController;
use App\Http\Controllers\ProblemDashboardController;
use App\Http\Controllers\CouncelingController;
use App\Http\Controllers\NewsAndUpdateController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\InboxController;
use App\Http\Controllers\AccountForumController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'landing'])->name('home');


Route::get('/login', function () {
    return view('Auth.index');
})->name('login');

Route::get('/activities', function () {
    return view('Landing.activities');
})->name('activities');

Route::get('/visionMission', function () {
    return view('Landing.visionMission');
})->name('visionMission');

Route::get('/newsupdates', function () {
    return view('Landing.newsupdates');
})->name('newsupdates');

Route::get('/strands', function () {
    return view('Landing.strands');
})->name('strands');

Route::get('/Organization', function () {
    return view('Landing.Organization');
})->name('Organization');

Route::get('/aboutus', function () {
    return view('Landing.aboutus');
})->name('aboutus');

Route::get('/registration', function () {
    return view('Auth.register');
})->name('registration');


Route::get('/forget-password', function () {
    return view('Auth.forgetPassword');
})->name('forget_password');

//sample for reset password
Route::get('/reset-password/{user_id?}', function () {
    return view('Auth.ResetPassword');
})->name('reset-password');

//Authentications/Login
Route::group(['prefix'=> 'auth', 'as' => 'auth.'], function(){
	// For Login
	Route::controller(AuthController::class)->group(function () {
 		 Route::post('request', 'authenticate')->name('authenticate');
 		 Route::get('forget-password-send', 'forgetPassword')->name('forget_password_send');
 		 Route::post('reset-password-send', 'resetPassword')->name('reset_password_send');
	});
});


Route::controller(UserController::class)->group(function(){
	Route::group(['prefix'=> 'users', 'as' => 'users.'], function(){
		 Route::get('', 'index')->name('index');
		 Route::get('list', 'list')->name('list');
		 Route::get('find/{user_id}', 'find')->name('find');
		 Route::post('add', 'add')->name('add');
		 Route::delete('delete/{user_id}', 'trash')->name('delete');
	});
});

Route::group(['middleware' => ['auth']], function(){
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    // staff management
    Route::controller(StaffController::class)->group(function(){
		Route::group(['prefix'=> 'students', 'as' => 'students.'], function(){
			 Route::get('', 'index')->name('index');
			 Route::get('list', 'list')->name('list');
			 Route::get('find/{student_id?}', 'find')->name('find');
			 Route::post('add', 'add')->name('add');
			 Route::delete('delete/{student_id}', 'trash')->name('delete');
		});
	});


    //Students management
	Route::controller(UserAccountController::class)->group(function(){
		Route::group(['prefix'=> 'user_accounts', 'as' => 'user_accounts.'], function(){
			 Route::get('/students', 'index')->name('index');
			 Route::get('list', 'list')->name('list');
			 Route::get('find/{user_id}', 'find')->name('find');
			 Route::post('add', 'add')->name('add');
			 Route::delete('delete/{user_id}', 'trash')->name('delete');
			 Route::get('approve', 'approve')->name('approve');
			 Route::get('disapprove', 'disapprove')->name('disapprove');

		});
	});

	//staff management
	Route::controller(StaffController::class)->group(function(){
		Route::group(['prefix'=> 'staff', 'as' => 'staff.'], function(){
			 Route::get('/students', 'index')->name('index');
			 Route::get('list', 'list')->name('list');
			 Route::get('find/{user_id}', 'find')->name('find');
			 Route::post('add', 'add')->name('add');
			 Route::delete('delete/{user_id}', 'trash')->name('delete');
		});
	});

    Route::get('dashboard', [DashboardController::class, 'index'])->name('index');



});

Route::group(['as' => 'admin.', 'middleware' => ['auth']], function(){
    Route::get('dashboard', [DashboardController::class, 'index'])->name('index');
});


Route::group(['prefix'=> 'account', 'as' => 'account.',  'middleware' => ['auth']], function(){
    Route::get('', [AccountController::class, 'index'])->name('index');
    Route::get('find/{id?}', [AccountController::class, 'find'])->name('find');
    Route::get('edit', [AccountController::class, 'edit'])->name('edit');
});

//problem
Route::group(['prefix' => 'problems', 'as' => 'problems.'], function(){
	Route::controller(ProblemController::class)->group(function () {
		Route::get('', 'index')->name('index');
		Route::get('list', 'list')->name('list');
		Route::post('save/{id?}', 'save')->name('save');
		Route::get('find/{id?}', 'find')->name('find');
		Route::delete('delete/{id?}', 'delete')->name('delete');
	});
});


Route::group(['prefix'=> 'student-profile', 'as' => 'student-profile.',  'middleware' => ['auth']], function(){
    Route::get('', [StudentProfileController::class, 'index'])->name('index');
    Route::get('/change-password', [StudentProfileController::class, 'changePassword'])->name('changePassword');
    Route::get('/change', [StudentProfileController::class, 'change'])->name('change');
    Route::post('/upload', [StudentProfileController::class, 'upload'])->name('upload');
   
});



Route::group(['prefix'=> 'counseling', 'as' => 'counseling.',  'middleware' => ['auth']], function(){
    Route::get('{id?}', [CouncelingController::class, 'index'])->name('index');
});

Route::controller(ProblemDashboardController::class)->group(function(){
	Route::group(['prefix'=> 'problem-dashboard', 'as' => 'problem-dashboard.'], function(){
		 Route::get('', 'index')->name('index');
		 Route::get('active', 'active')->name('active');
		 Route::get('list-active', 'listActive')->name('list_active');
		 Route::get('list', 'list')->name('list');
		 Route::get('find/{id}', 'find')->name('find');
		 Route::post('add', 'add')->name('add');
		 Route::delete('delete/{id}', 'trash')->name('delete');
		 Route::get('approve', 'approve')->name('approve');
		 Route::get('disapprove', 'disapprove')->name('disapprove');
		 Route::get('seen', 'seen')->name('seen');
		 Route::get('list-solve', 'listSolve')->name('list_solve');
		 Route::get('solve', 'solve')->name('solve');
	});
});


Route::controller(NewsAndUpdateController::class)->group(function(){
	Route::group(['prefix'=> 'news_and_update', 'as' => 'news_and_update.'], function(){
		 Route::get('', 'index')->name('index');
    	 Route::get('/activities', [NewsAndUpdateController::class, 'activities'])->name('activities');
    	 Route::get('/news', [NewsAndUpdateController::class, 'news'])->name('news');
	});
});

Route::controller(ForumController::class)->group(function(){
	Route::group(['prefix'=> 'forum', 'as' => 'forum.'], function(){
		 Route::get('/{id?}', 'index')->name('index');
	});
});

Route::get('send', [ForumController::class, 'send'])->name('message.send');
Route::get('list', [ForumController::class, 'list'])->name('forum.list');


Route::controller(AccountForumController::class)->group(function(){
	Route::group(['prefix'=> 'account-forum', 'as' => 'account-forum.'], function(){
		 Route::get('/{id?}', 'index')->name('index');
	});
});

Route::get('account-send', [AccountForumController::class, 'send'])->name('accountMessage.send');
Route::get('account-list', [AccountForumController::class, 'list'])->name('accountForum.list');
Route::get('account-solve', [AccountForumController::class, 'solve'])->name('accountForum.solve');


Route::controller(InboxController::class)->group(function(){
	Route::group(['prefix'=> 'account-inbox', 'as' => 'account-inbox.'], function(){
		 Route::get('', 'index')->name('index');
		 Route::get('/seen-user/{id?}', 'seenUser')->name('seen');
    	
	});
});


