@extends('Layout.admin_app')
@section('title', 'Staff Management')
@section('content')
<div class="row pt-3">
	<div class="col-lg-12 col-md-12 col-12">
		<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
			<div class="mb-3 mb-md-0">
				<h1 class="mb-1 h2 fw-bold">Staff accounts</h1>
			</div>
			<div class="d-flex">
				  <button class="btn btn-primary btn-shadow" onclick="add_staff();"><i class="fa fa-plus"></i> Add Staff accounts</button>
			</div>
		</div>
	</div>
</div>

<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered dt-responsive nowrap" id="tbl_staff" style="width: 100%;"></table>
		</div>
	</div>
</div>

<form class="needs-validation" id="user_account_form" action="{{ url('staff/add') }}" novalidate>
	<div class="modal fade" role="dialog" id="modal_staff" data-bs-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">
					Add staff
				</div>
				<button class="close" data-dismiss="modal" id="close_modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="user_id" name='user_id' placeholder="" class="form-control" required>
					<div class="position-relative mb-2 col-md-6">
						<label>First Name </l="abel>
						<input type="text" id="first_name" name="first_name" placeholder="Enter firstname here" class="form-control " required>
						<div class="invalid-feedback" id="err_first_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Middle Name </label>
						<input type="text" id="middle_name" name="middle_name" placeholder="Enter middlename here" class="form-control " required>
						<div class="invalid-feedback" id="err_middle_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label>Last Name </label>
						<input type="text" id="last_name" name="last_name" placeholder="Enter lastname here" class="form-control " required>
						<div class="invalid-feedback" id="err_last_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Gender </label>
						<select id="gender" name="gender" class="form-control">
							<option value="" selected>Please select gender</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Contact </label>
						<input type="text" id="contact" name="contact" placeholder="Enter contact number here" class="form-control " required>
						<div class="invalid-feedback" id="err_contact"></div>
					</div>
					
					<div class="position-relative mb-2 col-md-6">
						<label>Province </label>
						<input type="text" id="province" name="province" placeholder="Enter province here" class="form-control " required>
						<div class="invalid-feedback" id="err_province"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>City </label>
						<input type="text" id="city" name="city" placeholder="Enter city here" class="form-control " required>
						<div class="invalid-feedback" id="err_city"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label>Address </label>
						<input type="text" id="address" name="address" placeholder="Enter address here" class="form-control " required>
						<div class="invalid-feedback" id="err_address"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label>Email </label>
						<input type="email" id="email" name="email" placeholder="Enter email here" class="form-control " required>
						<div class="invalid-feedback" id="err_email"></div>
					</div>
					<div class="position-relative mb-2 col-md-12" id="password-div">
						<label>Password </label>
						<input type="password" id="password" name="password" class="form-control " required placeholder="Enter password here">
						<div class="invalid-feedback" id="err_password"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer d-block">
				<div class="row">
					<div class="col-sm-6 col-12">
						<button class="btn btn-dark col-sm-12 col-12" data-bs-dismiss="modal" type="button" id="cancel_btn">Cancel</button>
					</div>
					<div class="col-sm-6 col-12">
						<button class="btn btn-success col-sm-12 col-12" id="btn_submit_staff" type="submit">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
@endsection
@section('script')
<!-- Javascript Function-->
<script>
$('#cancel_btn').on('click', function(){
		
		$('#user_account_form')[0].reset();
});
	show_staff();
	var tbl_staff;
	function show_staff(){
		if (tbl_staff) {
			tbl_staff.destroy();
		}
		var url = main_path + 'staff/list';
		tbl_staff = $('#tbl_staff').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "first_name",
		"title": "First name",
	},{
		className: '',
		"data": "middle_name",
		"title": "Middle name",
	},{
		className: '',
		"data": "last_name",
		"title": "Last name",
	},{
		className: '',
		"data": "gender",
		"title": "Gender",
	},{
		className: '',
		"data": "contact",
		"title": "Contact",
	},{
		className: '',
		"data": "email",
		"title": "Email",
	},{
		className: 'width-option-1 text-center',
		"data": "user_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-id=\' '+row.user_id+'\' onclick="edit_staff(this)" type="button"><i class="fa fa-edit"></i></button> ';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.user_id+'\' onclick="delete_staff(this)" type="button"><i class="fa fa-trash"></i></button>';
				return newdata;
			}
		}
	]
	});
	}
	$("#user_account_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit_staff').prop('disabled', true);
					$('#btn_submit_staff').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'user_account_form');
					show_staff();
					$('#modal_staff').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'user_account_form');
				}
				$('#btn_submit_staff').prop('disabled', false);
				$('#btn_submit_staff').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});
	function delete_staff(_this){
		var id = $(_this).attr('data-id');
		var url =  main_path + 'staff/delete/' + id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this staff?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_staff();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}
	function add_staff(){
		$('#password-div').show();
		$('#modal_title_staff').text('Add User accounts');
		$('#user_id').val('');
		showValidator([],'user_account_form');
		$('#user_account_form').removeClass('was-validated');
		$('#user_id').val('');
		$('#first_name').val('');
		$('#middle_name').val('');
		$('#last_name').val('');
		$('#gender').val('');
		$('#contact').val('');
		$('#province').val('');
		$('#city').val('');
		$('#brgy').val('');
		$('#address').val('');
		$('#email').val('');
		$('#password').val('');
		$('#modal_staff').modal('show');
	}
	function edit_staff(_this){
		$('#modal_title_staff').text('Edit User accounts');
		
		var id = $(_this).attr('data-id');
		var url =  main_path + 'staff/find/' + id;
		$.ajax({
		type:"get",
		url:url,
		data:{},
		dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			let data = response.data;
			$('#user_id').val(data.user_id);
			$('#user_id').val(data.user_id);
			$('#first_name').val(data.first_name);
			$('#middle_name').val(data.middle_name);
			$('#last_name').val(data.last_name);
			$('#contact').val(data.contact);
			$('#province').val(data.province);
			$('#city').val(data.city);
			$('#brgy').val(data.brgy);
			$('#address').val(data.address);
			$('#email').val(data.email);
			$('#gender').val(data.gender);
			$('#password-div').hide();
			$('#modal_staff').modal('show');
		},
		error: function(error){
			console.log(error);
		}
		});
	}
</script>
@endsection
