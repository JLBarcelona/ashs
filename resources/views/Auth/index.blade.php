@extends('Layout.app')
@section('title', 'Login')
@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-4 col-lg-3 col-12">
			<form class="needs-validation" id="login_form" action="{{ route('auth.authenticate') }}" novalidate>
				<div class="card">
					<div class="card-body">
							<!-- input start -->
							<div class="row">
								<!-- form label end -->
								<div class="col-sm-12 col-12 form-group">
									<label for="">Email Address</label>
									<input type="email" class="form-control" name="email" id="email" aria-describedby="basic-addon1" placeholder="Email">
							    	  <div class="invalid-feedback" id="err_email"></div>
								</div>
								<div class="col-sm-12 col-12">
									<label for="">Password</label>
									<div class="input-group mb-3">
									 <input type="password" class="form-control" name="password" id="password" aria-describedby="basic-addon2" autocomplete="off" placeholder="Password">
									   <div class="input-group-append input-label">
									    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password');"></i></span>
									  </div>
									  <div class="invalid-feedback" id="err_password"></div> 
									</div>
								</div>
								<div class="col-sm-12 mb-2 text-center">
								<a href="{{ route('forget_password') }}">Forget Password?</a>
								</div>
							</div>
							<!-- input end -->
							<!-- button start -->
							<div class="row justify-content-end mb-2">
								<div class="col-sm-12 text-right mb-2">
									<button type="submit" class="btn btn-primary px-5 col-12" id="submit_button">Login</button>				
									<a href="{{ route('registration') }}" class="btn btn-success px-5 col-12 mt-2">Sign Up</a>			
								</div>
							</div>
							<!-- button end -->
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection


@section('script')
<script type="text/javascript">
	$("#login_form").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		         $("#submit_button").prop('disabled', false);
		        if(response.status == true){
		           showValidator(response.error,'login_form');
		           window.location = response.redirect;
		        }else{
		            showValidator(response.error,'login_form');
		        }
		    },
		    error:function(error){
		        console.log(error);
		        $("#submit_button").prop('disabled', false);
		    }
		});
	});
</script>

@endsection
