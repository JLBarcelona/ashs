@extends('Layout.app')
@section('title', 'Forget Password')
@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-lg-5 col-12">
			<form class="needs-validation" id="forget_password_form" action="{{ route('auth.forget_password_send') }}" novalidate>
				<div class="card">
					<div class="card-header"><p class="text-lg"><strong>Find your account</strong></p></div>
					<div class="card-body">
						<div class="col-12">
							<p class="text-lg">Enter your email to retrieve your password</p>
							<input type="email" name="email" id="email" placeholder="Enter your email address" class="form-control p-4">
							<div class="invalid-feedback" id="err_email"></div>
						</div>
					</div>
					<div class="card-footer text-right">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection


@section('script')
<script type="text/javascript">
	$('#forget_password_form').on('submit', function(e){
	e.preventDefault();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
		$.ajax({
		    type:"get",
		    url:url,
		    data:formData,
		    dataType:'json',
		    beforeSend:function(){
		    	Swal.fire('Please wait');
				Swal.showLoading();
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        swal("Success", response.message, "success");
		         $('#forget_password_form')[0].reset();
		     }else{
		      showValidator(response.error,'registration_form');
		      swal("Error", response.message, "error");
		      console.log(response);
		     }
		     Swal.close();
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	});
</script>

@endsection
