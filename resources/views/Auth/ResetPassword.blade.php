@extends('Layout.app')
@section('title', 'Reset Password')
 <style type="text/css">
    .input-group-text.bg-secondary{
      width: 145px;
    }
  </style>
@section('content')
    <form action="{{ route('auth.reset_password_send') }}" novalidate="" id="reset_password_form" class="needs-validation">
      <div class="container-fluid pt-5">
        <div class="row justify-content-center">
          <div class="col-sm-8">
          <div class="card">
            <div class="card-header text-primary h5">Create New Password</div>
            <div class="card-body">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary" style="">New Password</span>
                </div>
                <input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="Enter new password " aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                  <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye2', 'new_pass');"><i class="fa-solid fa-eye" id="eye2"></i></span>
                </div>
                <div class="invalid-feedback" id="err_new_pass"></div>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary" style="">Confirm Password</span>
                </div>
                <input type="password" id="confirm_pass" name="confirm_pass" class="form-control" placeholder="Confirm password " aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                  <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye3', 'confirm_pass');"><i class="fa-solid fa-eye" id="eye3"></i></span>
                </div>
                <div class="invalid-feedback" id="err_confirm_pass"></div>
              </div>
            </div>  
            <div class="card-footer text-right">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </div>
        </div>
        </div>
        </div>
      </div>
    </div>
  </form>
@endsection
@section('script')
<script type="text/javascript">
	 function password_visibile(icon, input){
    $("#"+icon).toggleClass('fa-eye fa-eye-slash');
    $("#"+input).attr('type', function(index, attr){
      return (attr == 'text')? 'password' : 'text';
    });
  }

  $('#reset_password_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    let url  = $(this).attr('action');
    $.ajax({
        type:"POST",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            swal("Success", response.message, "success");
             $('#reset_password_form')[0].reset();
         }else{
            swal("Error", response.message, "error");
          showValidator(response.error,'reset_password_form');
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });
</script>
@endsection
