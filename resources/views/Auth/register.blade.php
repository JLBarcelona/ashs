@extends('Layout.app')
@section('title', 'Sign Up')
@section('css')
<style type="text/css">
	.label:before{
     content:"*" ;
     color:red   
     }

</style>
@endsection
@section('content')
<div class="row justify-content-center">
	<div class="col-md-10 mt-5">
		<form class="needs-validation" id="registration_form" action="{{ url('users/add') }}" novalidate>
			<div class="card">
				<div class="card-header bg-primary h2">Registration form</div>
				<div class="card-body">
					<div class="row">
					<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">LRN number</label>
						<input type="text" id="student_no" name="student_no" placeholder="Enter LRN number here" class="form-control " required>
						<div class="invalid-feedback" id="err_student_no"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">First Name </label>
						<input type="text" id="first_name" name="first_name" placeholder="Enter firstname here" class="form-control " required>
						<div class="invalid-feedback" id="err_first_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Middle Name </label>
						<input type="text" id="middle_name" name="middle_name" placeholder="Enter middlename here" class="form-control " required>
						<div class="invalid-feedback" id="err_middle_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Last Name </label>
						<input type="text" id="last_name" name="last_name" placeholder="Enter lastname here" class="form-control " required>
						<div class="invalid-feedback" id="err_last_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Gender </label>
						<select id="gender" name="gender" class="form-control ">
							<option value="" selected>Please select gender</option>
							<option value="Male">Male</option>
							<option value=" Female">Female</option>
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Contact </label>
						<input type="text" id="contact" name="contact" placeholder="Enter contact number here" class="form-control " required>
						<div class="invalid-feedback" id="err_contact"></div>
					</div>

					<div class="position-relative mb-2 col-md-6">
						<label class="label">Grade </label>
						<select id="grade" name="grade" class="form-control ">
							<option value="" selected>Please select grade</option>
							<option value="Grade - 11">Grade - 11</option>
							<option value="Grade - 12">Grade - 12</option>
						</select>
						<div class="invalid-feedback" id="err_grade"></div>
					</div>
					
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Section</label>
						<input class="form-control" list="datalistOptions" id="section" name="section" placeholder="Type to search...">
						<datalist id="datalistOptions">
						  <option value="Grade 11 - Turqouise"></option>
						  <option value="Grade 11 - Alexandrite"></option>
						  <option value="Grade 11 - Quartz"></option>
						  <option value="Grade 11 - Pearl"></option>
						  <option value="Grade 11 - Beryl"></option>
						  <option value="Grade 11 - Amber"></option>
						  <option value="Grade 11 - Neprite"></option>
						  <option value="Grade 11 - Morganite"></option>
						  <option value="Grade 11 - Emerald"></option>
						  <option value="Grade 11 - Diamond"></option>
						  <option value="Grade 11 - Amethyst"></option>
						  <option value="Grade 12 - Kelvin"></option>
						  <option value="Grade 12 - Fahregnheit"></option>
						  <option value="Grade 12 - Celcius"></option>
						  <option value="Grade 12 - Rankine"></option>
						  <option value="Grade 12 - Aurelius"></option>
						  <option value="Grade 12 - Freud"></option>
						  <option value="Grade 12 - Dewey"></option>
						  <option value="Grade 12 - Augustine"></option>
						  <option value="Grade 12 - Hobbes"></option>
						  <option value="Grade 12 - Kant"></option>
						  <option value="Grade 12 - Sartre"></option>
						  <option value="Grade 12 - Aristotle"></option>
						  <option value="Grade 12 - Rand"></option>
						  <option value="Grade 12 - Socrates"></option>
						  <option value="Grade 12 - Lao Tzu"></option>
						  <option value="Grade 12 - Mencius"></option>
						</datalist>
						<div class="invalid-feedback" id="err_section"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Strand </label>
						<select id="strand" name="strand" class="form-control ">
							<option value="" selected disabled="">Please select strand</option>
							<option value="TVL">Technical Vocational & Livelihood (TVL)</option>
							<option value="ABM">Accountancy Business and Management (ABM)</option>
							<option value="HUMSS">Humanities and Social Sciences (HUMSS)</option>
							<option value="STEM">Science Technology, Engineering and Mathematics (STEM)</option>
						</select>
						<div class="invalid-feedback" id="err_strand"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Province </label>
						<input type="text" id="province" name="province" placeholder="Enter province here" class="form-control " required>
						<div class="invalid-feedback" id="err_province"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">City </label>
						<input type="text" id="city" name="city" placeholder="Enter city here" class="form-control " required>
						<div class="invalid-feedback" id="err_city"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Barangay </label>
						<input type="text" id="barangay" name="barangay" placeholder="Enter barangay here" class="form-control " required>
						<div class="invalid-feedback" id="err_barangay"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Address </label>
						<input type="text" id="address" name="address" placeholder="Enter address here" class="form-control " required>
						<div class="invalid-feedback" id="err_address"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label>Guardian Name </label>
						<input type="text" id="guardian_name" name="guardian_name" placeholder="Enter guradian name here" class="form-control " required>
						<div class="invalid-feedback" id="err_guradian_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Email </label>
						<input type="email" id="email" name="email" placeholder="Enter email here" class="form-control " required>
						<div class="invalid-feedback" id="err_email"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Password </label>
						<input type="password" id="password" name="password" placeholder="Enter password here" class="form-control " required>
						<div class="invalid-feedback" id="err_password"></div>
					</div>
				</div>
				</div>
				<div class="card-footer text-right">
					<button class="btn btn-secondary" type="button" id="clear_form">Clear</button>
					<button class="btn btn-success" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection

@section('script')
<!-- Javascript Function-->
<script>
	$("#registration_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit_users').prop('disabled', true);
					$('#btn_submit_users').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'registration_form');
					$('#registration_form').trigger("reset");
					$('#modal_users').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'registration_form');
				}
				$('#btn_submit_users').prop('disabled', false);
				$('#btn_submit_users').text('Save');
			},
			error:function(error){
				console.log(error)

			}
		});
	});


	$('#clear_form').on('click', function(e){
		clear_form();
	});
</script>
@endsection
