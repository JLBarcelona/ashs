@extends('Layout.admin_app')
@section('title', 'Forum')
@section('css')
<style type="text/css">
	.card-body{
		

	}
	.card{
		border: solid #1E90FF 2px;
		background-color: #FFFFF0;
	}
	.col-sm-6{
		border-radius: 15px
	}
	.input-message{
		border: solid #1E90FF;
		border-radius: 10px
	}
	.label_other{
		border: solid #DCDCDC 10px;
		border-radius: 20px;
		color: black;
		background-color: #DCDCDC ;
	}
	.label_self{
		border: solid #1E90FF 10px;
		border-radius: 20px;
		color: white;
		background-color: #1E90FF;
	}
	.scroll {
    max-height: 60vh;
    overflow-y: auto;
    
}
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row pt-2">
		<div class="col-sm-12">
			<center><h3 style="color: navy">Messaging</h3></center>
			<div class="card">
				<div class="card-header">
					<label class="text-lg">{{$user}}: </label>
					<label class="text-danger text-lg">{{$prob}}</label>
				</div>
				<div class="card-body scroll" id="forum_data_body">	
					
				</div>
					<div class="card-footer bg-transparent">

					<div class="row mt-3">
						<div class="col-sm-11">
							<textarea type="text" name="message" id="message" class="form-control input-message" rows="1" placeholder="Aa"></textarea>
						</div>
						<div class="col-sm-1">
							<button class="btn btn-primary px-4" type="button" onclick="send_message()"><i class="fas fa-paper-plane"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
<script type="text/javascript">
	get_forum_data();
	
	setInterval(function(){
		get_forum_data();
	}, 5000);

	function get_forum_data(){
		$.ajax({
			type:'get',
			url:"{{ route('forum.list') }}",
			data:{},
			dataType:'json',
			success:function(response){
				let responseData = response.data;
				//console.log(response);
				let table_output = responseData.map(function(usr){
					let out = '';
					if (usr.sender_id) {
					out+='<div class="row justify-content-end text-right mb-3">';
					out+='<div class="col-sm-6">';
					out+='<label class="label_self px-2">'+usr.message+'</label>';
					out+='</div>';
					out+='<div class="col-sm-12 text-center">';
					// out+='<label class="text-secondary text-sm">'+usr.created_at+'</label>';
					out+='</div>';
					out+='</div>';
					}else{
					out+='<div class="row mb-3">';
					out+='<div class="col-sm-6">';
					out+='<label class="label_other px-2">'+usr.message+'</label><br>';
					out+='</div>';
					out+='<div class="col-sm-12 text-center">';
					// out+='<label class="text-secondary text-sm">'+usr.created_at+'</label>';
					out+='</div>';
					out+='</div>';
					}
					return out;
				}).join('');

				$("#forum_data_body").html(table_output);
			}
		});
	}


function send_message(){
	if ( $('#message').val() == '' ) {
	}else{
		var messages = $('#message').val();
	$.ajax({
	    type:"GET",
	    url:"{{ route('message.send') }}",
	    data:{messages : messages},
	    dataType:'json',
	    beforeSend:function(){
	    },
	    success:function(response){
	      // console.log(response);
	     if (response.status == true) {
	        get_forum_data();
	        $('#message').val('');
	     }else{
	      console.log(response);
	     }
	    },
	    error: function(error){
	      console.log(error);
	    }
	  });
	}
} 



</script>
@endsection
