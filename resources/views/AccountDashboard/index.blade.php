@extends('Layout.account.app')
@section('title', 'Account')
@section('content')
	 <!-- Main content -->
      <div class="container-fluid pt-3">
        <div class="row">
          <div class="col-lg-4 col-4">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $students }}</h3>
                <p>Students</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-contacts"></i>
              </div>
              <a href="#" class="small-box-footer" style="padding-bottom: 27px"></a>
            </div>
          </div>
          <div class="col-lg-4 col-4">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>2</h3>
                <p>News and Updates</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-briefcase-outline"></i>
              </div>
              <a href="{{ route('home')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{ $inbox}}</h3>
                <p>Inbox</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope"></i>
              </div>
              <a href="{{ route('account-inbox.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
           <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{ $pending }}</h3>
                <p>Counseling Request</p>
              </div>
              <div class="icon">
                <i class="fa fa-handshake"></i>
              </div>
              <a href="{{ route('problems.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          

          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{ $active}}</h3>
                <p>Active Counseling</p>
              </div>
              <div class="icon">
                <i class="fa-solid fa-inbox"></i>


              </div>
              <a href="{{ route('problems.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $solve}}</h3>
                <p>Solve Counseling</p>
              </div>
              <div class="icon">
                <i class="fa-regular fa-square-check"></i>
              </div>
              <a href="{{ route('problems.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
@endsection
@section('script')

@endsection
