@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
 <!-- Main content -->

      <div class="container-fluid pt-3">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$students}}</h3>

                <p>Students</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-contacts"></i>
              </div>
              <a href="{{ route('user_accounts.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-6 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$staff}}</h3>

                <p>Staffs</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-briefcase-outline"></i>
              </div>
              <a href="{{ route('staff.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
           <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$pending}}</h3>

                <p>Counseling Request</p>
              </div>
              <div class="icon">
                <i class="fa fa-handshake"></i>
              </div>
              <a href="{{ route('problem-dashboard.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{$active}}</h3>

                <p>Active Counseling</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope"></i>

              </div>
              <a href="{{ route('problem-dashboard.active') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$solve}}</h3>

                <p>Solved Counseling</p>
              </div>
              <div class="icon">
                <i class="fa-regular fa-square-check"></i>

              </div>
              <a href="{{ route('problem-dashboard.solve') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Student Analytics</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
               
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="text-center h2">Gender</div>
                    <div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                  </div>

                  <div class="col-md-4">
                    <div class="text-center h2">Grade</div>
                    <div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="donutChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                  </div>

                  <div class="col-md-4">
                    <div class="text-center h2">Strand</div>
                    <div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="donutChart3" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                  </div>

                  <div class="col-md-12">
                    <div class="text-center h2">Section</div>
                    <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                      <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 487px;" width="487" height="250" class="chartjs-render-monitor"></canvas>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- ./col -->
         
          <!-- ./col -->
          
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
@endsection

@section('script')
<script src="{{ asset('adminLTE/plugins/chart.js/Chart.min.js') }}"></script>

<script type="text/javascript">
    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Male',
          'Female',
      ],
      datasets: [
        {
          data: ['{{ get_student_gender()['male'] }}','{{ get_student_gender()['female'] }}'],
          backgroundColor : ['#00c0ef',  '#f54797'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }

      //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    });



    var donutChartCanvas2 = $('#donutChart3').get(0).getContext('2d')
    var donutData2        = {
      labels: [
          'TVL',
          'ABM',
          'HUMSS',
          'STEM',
      ],
      datasets: [
        {
          data: [
          '{{ get_student_strands()['TVL'] }}',
          '{{ get_student_strands()['ABM'] }}',
          '{{ get_student_strands()['HUMSS'] }}',
          '{{ get_student_strands()['STEM'] }}',
          ],
          backgroundColor : ['#00a65a','#f56954',  '#00c0ef', '#f39c12', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
    var donutOptions2     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    new Chart(donutChartCanvas2, {
      type: 'doughnut',
      data: donutData2,
      options: donutOptions2
    });


     var donutChartCanvas2 = $('#donutChart2').get(0).getContext('2d')
    var donutData2        = {
      labels: [
          'Grade 11',
          'Grade 12',
      ],
      datasets: [
        {
          data: ['{{ get_student_grades()['eleven'] }}','{{ get_student_grades()['twelve'] }}'],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
    var donutOptions2     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    new Chart(donutChartCanvas2, {
      type: 'doughnut',
      data: donutData2,
      options: donutOptions2
    });


     var areaChartData = {
      labels  : JSON.parse('{!! json_encode(get_student_per_section()['label']) !!}'),
      datasets: [
        {
          label               : 'Girls',
          backgroundColor     : '#f54797',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : JSON.parse('{!! json_encode(get_student_per_section()['data_girls']) !!}')
        },
        {
          label               : 'Boys',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : JSON.parse('{!! json_encode(get_student_per_section()['data_boys']) !!}')
        },
      ]
    }
  
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

</script>
@endsection