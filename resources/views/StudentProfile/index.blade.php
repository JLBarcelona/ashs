@extends('Layout.account.app')
@section('title', 'Profile-Settings')
@section('content')
	<div class="container pt-4">
		<div class="main-body">
			<div class="row">
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">
							<div class="d-flex flex-column align-items-center text-center">
								@if(empty(Auth::user()->user_image))
								<img src="{{ asset('img/dp.jpg')}}" alt="Admin" class="rounded-circle p-1 bg-secondary"  style="cursor: pointer; width: 200px; height: 200px" onclick="upload_img()">
								@else
								<img src="{{ asset('storage/'.Auth::user()->user_image)}}" alt="Admin" class="rounded-circle p-1 bg-secondary"  style="cursor: pointer; width: 200px; height: 200px" onclick="upload_img()">
								@endif
								<div class="mt-3" style="margin-bottom: 120px">
									<h4>{{ Auth::user()->first_name.' '.Auth::user()->last_name}}</h4>
									<h6>Student number: {{ Auth::user()->student_no}}</h6>
									<button class="btn btn-primary col-sm-12 px-5 mt-2" type="button" onclick="open_edit_modal('{{Auth::user()->user_id}}')">Edit Account</button>
									<a href="{{ route('student-profile.changePassword') }}">
									<button type="button" class="btn btn-success mt-2 px-5 col-sm-12">Change Password</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="card">
						<div class="card-body">
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Full Name</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{ Auth::user()->last_name.', '.Auth::user()->first_name.', '.Auth::user()->middle_name}}.">
								</div>
							</div>
							<hr>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Contact</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{ Auth::user()->contact}}">
								</div>
							</div>
							<hr>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Grade And Section</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{ Auth::user()->section}}">
								</div>
							</div>
							<hr>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Gender</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{ Auth::user()->gender}}">
								</div>
							</div>
							<hr>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Strand</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{Auth::user()->strand}}">
								</div>
							</div>
							<hr>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Address</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{ Auth::user()->address}}">
								</div>
							</div>
							<hr>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mt-2">Email</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" readonly="" value="{{ Auth::user()->email}}">
								</div>
							</div>
							<hr>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	 <div class="modal fade" role="dialog" id="edit_account">
	      <div class="modal-dialog modal-lg">
	        <div class="modal-content">
	          <div class="modal-header">
	            <div class="modal-title">
	            Edit Your Profile
	            </div>
	            <button class="close" data-dismiss="modal">&times;</button>
	          </div>
	          <div class="modal-body">
	          	<form action="{{ route('account.edit')}}" id="edit_form" class="needs-validation" novalidate="">
	           <div class="row">
					<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">LRN number</label>
						<input type="text" id="student_no" name="student_no" placeholder="Enter LRN number here" class="form-control " required>
						<div class="invalid-feedback" id="err_student_no"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">First Name </label>
						<input type="text" id="first_name" name="first_name" placeholder="Enter firstname here" class="form-control " required>
						<div class="invalid-feedback" id="err_first_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Middle Name </label>
						<input type="text" id="middle_name" name="middle_name" placeholder="Enter middlename here" class="form-control " required>
						<div class="invalid-feedback" id="err_middle_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Last Name </label>
						<input type="text" id="last_name" name="last_name" placeholder="Enter lastname here" class="form-control " required>
						<div class="invalid-feedback" id="err_last_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Gender </label>
						<select id="gender" name="gender" class="form-control ">
							<option value="" selected>Please select gender</option>
							<option value="Male">Male</option>
							<option value=" Female">Female</option>
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Contact </label>
						<input type="text" id="contact" name="contact" placeholder="Enter contact number here" class="form-control " required>
						<div class="invalid-feedback" id="err_contact"></div>
					</div>
					
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Grade</label>
						<select id="grade" name="grade" class="form-control ">
							<option value="" selected>Please select grade</option>
							<option value="Grade 11">Grade 11</option>
							<option value="Grade 12"> Grade 12</option>
						</select>
						<div class="invalid-feedback" id="err_grade"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Section</label>
						<input class="form-control" list="datalistOptions" id="section" name="section" placeholder="Type to search...">
						<datalist id="datalistOptions">
						  <option value="Grade 11 - Turqouise"></option>
						  <option value="Grade 11 - Alexandrite"></option>
						  <option value="Grade 11 - Quartz"></option>
						  <option value="Grade 11 - Pearl"></option>
						  <option value="Grade 11 - Beryl"></option>
						  <option value="Grade 11 - Amber"></option>
						  <option value="Grade 11 - Neprite"></option>
						  <option value="Grade 11 - Morganite"></option>
						  <option value="Grade 11 - Emerald"></option>
						  <option value="Grade 11 - Diamond"></option>
						  <option value="Grade 11 - Amethyst"></option>
						  <option value="Grade 12 - Kelvin"></option>
						  <option value="Grade 12 - Fahregnheit"></option>
						  <option value="Grade 12 - Celcius"></option>
						  <option value="Grade 12 - Rankine"></option>
						  <option value="Grade 12 - Aurelius"></option>
						  <option value="Grade 12 - Freud"></option>
						  <option value="Grade 12 - Dewey"></option>
						  <option value="Grade 12 - Augustine"></option>
						  <option value="Grade 12 - Hobbes"></option>
						  <option value="Grade 12 - Kant"></option>
						  <option value="Grade 12 - Sartre"></option>
						  <option value="Grade 12 - Aristotle"></option>
						  <option value="Grade 12 - Rand"></option>
						  <option value="Grade 12 - Socrates"></option>
						  <option value="Grade 12 - Lao Tzu"></option>
						  <option value="Grade 12 - Mencius"></option>
						</datalist>
						<div class="invalid-feedback" id="err_section"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Strand </label>
						<select id="strand" name="strand" class="form-control ">
							<option value="" selected>Please select strand</option>
							<option value="TVL">Technical Vocational & Livelihood (TVL)</option>
							<option value="ABM">Accountancy Business and Management (ABM)</option>
							<option value="HUMSS">Humanities and Social Sciences (HUMSS)</option>
							<option value="STEM">Science Technology, Engineering and Mathematics (STEM)</option>
						</select>
						<div class="invalid-feedback" id="err_strand"></div>
					</div>
					<div class="position-relative mb-2 col-md-4">
						<label class="label">Province </label>
						<input type="text" id="province" name="province" placeholder="Enter province here" class="form-control " required>
						<div class="invalid-feedback" id="err_province"></div>
					</div>
					<div class="position-relative mb-2 col-md-4">
						<label class="label">City </label>
						<input type="text" id="city" name="city" placeholder="Enter city here" class="form-control " required>
						<div class="invalid-feedback" id="err_city"></div>
					</div>
					<div class="position-relative mb-2 col-md-4">
						<label class="label">Barangay </label>
						<input type="text" id="barangay" name="barangay" placeholder="Enter barangay here" class="form-control " required>
						<div class="invalid-feedback" id="err_barangay"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Address </label>
						<input type="text" id="address" name="address" placeholder="Enter address here" class="form-control " required>
						<div class="invalid-feedback" id="err_address"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label>Guardian Name </label>
						<input type="text" id="guardian_name" name="guardian_name" placeholder="Enter guradian name here" class="form-control " required>
						<div class="invalid-feedback" id="err_guradian_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Email </label>
						<input type="email" id="email" name="email" placeholder="Enter email here" class="form-control " required>
						<div class="invalid-feedback" id="err_email"></div>
					</div>
				</div>
			</div>
	          <div class="modal-footer">
	            <button type="button" class="px-4 btn btn-danger" onclick="clear_btn()">Clear</button>
	            <button type="submit" class="px-4 btn btn-success">Save</button>
	          </div>
	          </form>
	        </div>
	      </div>
	    </div>
	     <div class="modal fade" role="dialog" id="upload_img_modal">
	          <div class="modal-dialog">
	            <div class="modal-content">
	              <div class="modal-header">
	                <div class="modal-title">
	                Upload Profile Image
	                </div>
	                <button class="close" data-dismiss="modal">&times;</button>
	              </div>
	              <div class="modal-body">
	              	<form action="{{ route('student-profile.upload') }}" id="upload_form" novalidate="" class="needs-validation">
	                <div class="row">
	                	<div class="col-sm-12 text-center">
	                		@if(empty(Auth::user()->user_image))
								<img src="{{ asset('img/dp.jpg')}}" alt="Admin" class="bg-secondary img-thumbnail"  style="width: 350px; height: 350px" onclick="upload_img()">
								@else
								<img src="{{ asset('storage/'.Auth::user()->user_image)}}" alt="Admin" class="bg-secondary img-thumbnail"  style="width: 350px; height: 350px" onclick="upload_img()">
								@endif
	                	</div>
	                	<div class="col-sm-12 text-center mt-3">
	                		<label>Upload Image</label>
	                	</div>
	                	<div class="col-sm-12 text-center mt-3">
	                		<input type="file" name="user_image" id="user_image">
	                	</div>
	                </div>
	              </div>
	              <div class="modal-footer">
	                <button type="submit" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-success">Upload</button>
	                </form>
	              </div>
	            </div>
	          </div>
	        </div>

	     
@endsection
@section('script')
<script type="text/javascript">
$('#upload_form').on('submit', function(e){
  e.preventDefault();
  // let formData = $(this).serialize();
  let form = $("#upload_form")[0];
    // Create an FormData object 
  var data = new FormData(form);
  let url = $(this).attr('action');
  $.ajax({
      type:"POST",
      url:url,
      data:data,
      enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
         //console.log(response);
       if (response.status === true) {
          swal("Success", response.message, "success");
		  $('#upload_img_modal').modal('show');
          $('#upload_form')[0].reset();
          setTimeout(function(){
			 	location.reload();
			}, 700);

       }else{
          swal("error", response.message, "error");
        showValidator(response.error,'upload_form');
        console.log(response);
       }
      },
      error: function(error){
        console.log(error);
      }
    });

});


function upload_img(){
	$('#upload_img_modal').modal('show');
} 

function change_password(id){
	$('#change_pass').modal('show');
}

	$("#edit_form").on('submit', function(e){
		e.preventDefault(e);
		let url = $(this).attr('action');
		let formData = $(this).serialize();
		$.ajax({
		    type:"get",
		    url:url,
		    data:formData,
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		     if (response.status == true) {
		        swal("Success", response.message, "success");
		        setTimeout(function() {
		        	location.reload();
		        }, 600);
		     }else{
		       console.log(response);
		      showValidator(response.error,'edit_form');
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	});

	function clear_btn(){
		$('#edit_form')[0].reset();
	}

	function open_edit_modal(id){
		$.ajax({
		    type:"GET",
		    url:"{{ route('account.find')}}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		    	let data = response.data;
		    $('#user_id').val(data.user_id);
			$('#first_name').val(data.first_name);
			$('#middle_name').val(data.middle_name);
			$('#last_name').val(data.last_name);
			$('#contact').val(data.contact);
			$('#province').val(data.province);
			$('#city').val(data.city);
			$('#brgy').val(data.brgy);
			$('#address').val(data.address);
			$('#email').val(data.email);
			$('#student_no').val(data.student_no);
			$('#strand').val(data.strand);
			$('#grade').val(data.grade);
			$('#gender').val(data.gender);
			$('#strand').val(data.strand);
			$('#section').val(data.section);
			$('#grade').val(data.grade);
			$('#barangay').val(data.barangay);
			$('#guardian_name').val(data.guardian_name);

				$('#edit_account').modal('show');
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}
</script>
@endsection
