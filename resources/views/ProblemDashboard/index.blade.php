@extends('Layout.admin_app')
@section('title', 'Counseling Request')
@section('content')
<div class="container-fluid pt-4">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">List for Counseling Request</div>
        <div class="card-body">
          <table class="table table-bordered dt-responsive nowrap" id="table_problem_id" style="width: 100%;">
            
          </table> 
        </div>
        <div class="card-footer">
          
        </div>
      </div>
    </div>
  </div>  
</div>
@endsection

@section('script')
<script type="text/javascript">
  show_Users();
  var table_problem_id;
  function show_Users(){
    if (table_problem_id) {
      table_problem_id.destroy();
    }
    
    table_problem_id = $('#table_problem_id').DataTable({
    pageLength: 10,
    responsive: true,
    aaSorting: [],
    ajax: "{{ route('problem-dashboard.list')}}",
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "users.first_name",
    "title": "Name",
    "render": function(data, type, row, meta){
      return row.users.last_name+', '+row.users.first_name
    }
  },{
    className: '',
    "data": "problem_type",
    "title": "Problem Type",
  },{
    className: '',
    "data": "date",
    "title": "Date",
  }
  ,{
    className: 'width-option-1 text-center',
    "data": "status",
    "orderable": false,
    "title": "Status",
      "render": function(data, type, row, meta){
        newdata = '';
        if (data == 0) {
        newdata += ' <span class="badge badge-warning mt-1" style="width: 80px" type="button">Pending</i></span>';
        }else if(data == 1){
        newdata += ' <span class="badge badge-success mt-1" style="width: 80px" type="button">Approved</i></span>';
        }else if(data == 2){
        newdata += ' <span class="badge badge-danger mt-1" style="width: 80px" type="button">Declined</i></span>';
        }else{
        newdata += ' <span class="badge badge-danger mt-1" style="width: 80px" type="button">Solve</i></span>';
        }
        return newdata;
    }
    },
    {
    className: 'width-option-1 text-center',
    "data": "user_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        newdata = '';
        newdata += ' <button class="btn btn-info btn-sm font-base mt-1" onclick="approve('+row.id+', \'approve\')" type="button" title="Approve Request"><i class="fa fa-thumbs-up"></i></button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="disapprove('+row.id+', \'decline\')" type="button" title="Decline Request"><i class="fa fa-thumbs-down"></i></button>';
        return newdata;
      }
    }
  ]
  });
  }


function approve(id, status){
  swal({
        title: "Are you sure?",
        text: "Do you want to "+status+" this request ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
        function(){
         $.ajax({
          type:"GET",
          url:"{{ route('problem-dashboard.approve') }}",
          data:{problem_id : id, status : status},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              swal("Success", response.message, "success");
              show_Users();
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
      });
  }


function disapprove(id, status){
  swal({
        title: "Are you sure?",
        text: "Do you want to "+status+" this request ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
        function(){
         $.ajax({
          type:"GET",
          url:"{{ route('problem-dashboard.disapprove') }}",
          data:{problem_id : id, status : status},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              swal("Success", response.message, "success");
              show_Users();
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
      });
  }

  
</script>
@endsection
