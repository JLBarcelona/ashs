@extends('Layout.admin_app')
@section('title', 'Active-Counseling')
@section('content')
<div class="container-fluid pt-4">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">List of Active Counseling</div>
        <div class="card-body">
          <table class="table table-bordered dt-responsive nowrap" id="table_problem_id" style="width: 100%;">
            
          </table> 
        </div>
        <div class="card-footer">
          
        </div>
      </div>
    </div>
  </div>  
</div>

@endsection

@section('script')
<script type="text/javascript">
  show_Users();
  var table_problem_id;
  function show_Users(){
    if (table_problem_id) {
      table_problem_id.destroy();
    }
    
    table_problem_id = $('#table_problem_id').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: "{{ route('problem-dashboard.list_active')}}",
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "users.first_name",
    "title": "Name",
    "render": function(data, type, row, meta){
      return row.users.last_name+', '+row.users.first_name
    }
  },{
    className: '',
    "data": "problem_type",
    "title": "Problem Type",
  },{
    className: '',
    "data": "users.strand",
    "title": "Strand",
  },{
    className: 'text-center',
    "data": "date",
    "title": "Date Requested",
  }
  ,{
    className: 'width-option-1 text-center',
    "data": "forums_is_unseen_count",
    "orderable": false,
    "title": "Unread Message",
      "render": function(data, type, row, meta){
        return ' <span class="badge badge-danger mt-1" style="width: 80px" type="button">'+data+'</i></span>';
    }
    }
    ,
    {
    className: 'width-option-1 text-center',
    "data": "user_id",
    "orderable": false,
    "title": "Message",
      "render": function(data, type, row, meta){
        newdata = '';
        newdata += ' <button class="btn btn-primary btn-sm font-base mt-1" onclick="newpage('+row.id+', \'decline\')" type="button" title="Talk to Student"><i class="fa fa-envelope"></i></button>';
        return newdata;
      }
    }
  ]
  });
  }
  
  function newpage(id){
    window.location.href = "{{ route('forum.index') }}"+'/'+id;
    $.ajax({
        type:"GET",
        url:"{{ route('problem-dashboard.seen') }}",
        data:{id : id},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  }

</script>
@endsection
