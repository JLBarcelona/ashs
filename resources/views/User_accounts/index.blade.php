@extends('Layout.admin_app')
@section('title', 'Students Management')
@section('css')
<style type="text/css">
	.label:before{
     content:"*" ;
     color:red   
     }

</style>
@endsection
@section('content')
<div class="row pt-3">
	<div class="col-lg-12 col-md-12 col-12">
		<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
			<div class="mb-3 mb-md-0">
				<h1 class="mb-1 h2 fw-bold">Student accounts</h1>
			</div>
			<div class="d-flex">
				  <button class="btn btn-primary btn-shadow" onclick="add_user_accounts();"><i class="fa fa-plus"></i> Add Student accounts</button>
			</div>
		</div>
	</div>
</div>


		<form class="needs-validation" id="user_account_form" action="{{ route('user_accounts.add') }}" novalidate>
		<div class="col-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">Record Filter</h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
             
              <div class="card-body">

                  <div class="row">


                        <div class="position-relative mb-2 col-md-4">
						<label>Grade & Section</label>
						<select class="form-control" name="grade_and_section" id="grade_and_section" type="text" class="form-control">
						  <option value="" selected="">Please Select Grade & Section</option>
						  <option value="Grade 11 - Turqouise">Grade 11 - Turqouise</option>
						  <option value="Grade 11 - Alexandrite">Grade 11 - Alexandrite</option>
						  <option value="Grade 11 - Quartz">Grade 11 - Quartz</option>
						  <option value="Grade 11 - Pearl">Grade 11 - Pearl</option>
						  <option value="Grade 11 - Beryl">Grade 11 - Beryl</option>
						  <option value="Grade 11 - Amber">Grade 11 - Amber</option>
						  <option value="Grade 11 - Neprite">Grade 11 - Neprite</option>
						  <option value="Grade 11 - Morganite">Grade 11 - Morganite</option>
						  <option value="Grade 11 - Emerald">Grade 11 - Emerald</option>
						  <option value="Grade 11 - Diamond">Grade 11 - Diamond</option>
						  <option value="Grade 11 - Amethyst">Grade 11 - Amethyst</option>
						  <option value="Grade 12 - Kelvin">Grade 12 - Kelvin</option>
						  <option value="Grade 12 - Fahregnheit">Grade 12 - Fahregnheit</option>
						  <option value="Grade 12 - Celcius">Grade 12 - Celcius</option>
						  <option value="Grade 12 - Rankine">Grade 12 - Rankine</option>
						  <option value="Grade 12 - Aurelius">Grade 12 - Aurelius</option>
						  <option value="Grade 12 - Freud">Grade 12 - Freud</option>
						  <option value="Grade 12 - Dewey">Grade 12 - Dewey</option>
						  <option value="Grade 12 - Augustine">Grade 12 - Augustine</option>
						  <option value="Grade 12 - Hobbes">Grade 12 - Hobbes</option>
						  <option value="Grade 12 - Kant">Grade 12 - Kant</option>
						  <option value="Grade 12 - Sartre">Grade 12 - Sartre</option>
						  <option value="Grade 12 - Aristotle">Grade 12 - Aristotle</option>
						  <option value="Grade 12 - Rand">Grade 12 - Rand</option>
						  <option value="Grade 12 - Socrates">Grade 12 - Socrates</option>
						  <option value="Grade 12 - Lao Tzu">Grade 12 - Lao Tzu</option>
						  <option value="Grade 12 - Mencius">Grade 12 - Mencius</option>
						</select>
					</div>
					<div class="position-relative mb-2 col-md-4">
						<label>Strand</label>
						<select class="form-control" name="strand_filter" id="strand_filter" type="text" class="form-control">
						  	<option value="" selected>Please select strand</option>
							<option value="TVL">Technical Vocational & Livelihood (TVL)</option>
							<option value="ABM">Accountancy Business and Management (ABM)</option>
							<option value="HUMSS">Humanities and Social Sciences (HUMSS)</option>
							<option value="STEM">Science Technology, Engineering and Mathematics (STEM)</option>
						</select>
						
					</div>
                    
                  </div>
      
              </div>

              <div class="card-footer text-right">
                <button type="button" class="btn btn-secondary" data-card-widget="collapse">
                  <i class="fa fa-minus"></i> Minimize Filter
                </button>
                <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
                <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
              </div>
           
            </div>

          </div>

<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered dt-responsive nowrap" id="tbl_user_accounts" style="width: 100%;"></table>
		</div>
	</div>
</div>

	<div class="modal fade" role="dialog" id="modal_user_accounts" data-bs-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="modal_title_user_accounts">
					Add Students
				</div>
				<button class="close" data-dismiss="modal" id="onclose_modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">LRN number</label>
						<input type="text" id="student_no" name="student_no" placeholder="Enter LRN number here" class="form-control " required>
						<div class="invalid-feedback" id="err_student_no"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">First Name </label>
						<input type="text" id="first_name" name="first_name" placeholder="Enter firstname here" class="form-control " required>
						<div class="invalid-feedback" id="err_first_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Middle Name </label>
						<input type="text" id="middle_name" name="middle_name" placeholder="Enter middlename here" class="form-control " required>
						<div class="invalid-feedback" id="err_middle_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Last Name </label>
						<input type="text" id="last_name" name="last_name" placeholder="Enter lastname here" class="form-control " required>
						<div class="invalid-feedback" id="err_last_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Gender </label>
						<select id="gender" name="gender" class="form-control ">
							<option value="" selected disabled="">Please select gender</option>
							<option value="Male">Male</option>
							<option value=" Female">Female</option>
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Contact </label>
						<input type="text" id="contact" name="contact" placeholder="Enter contact number here" class="form-control " required>
						<div class="invalid-feedback" id="err_contact"></div>
					</div>

					<div class="position-relative mb-2 col-md-6">
						<label class="label">Grade</label>
						<select id="grade" name="grade" class="form-control ">
							<option value="" selected disabled="">Please select gender</option>
							<option value="Grade - 11">Grade - 11</option>
							<option value="Grade - 12">Grade - 12</option>
						</select>
						<div class="invalid-feedback" id="err_grade"></div>
					</div>
					
					
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Section</label>
						<input class="form-control" list="datalistOptions" id="section" name="section" placeholder="Type to search...">
						<datalist id="datalistOptions">
						  <option value="Grade 11 - Turqouise"></option>
						  <option value="Grade 11 - Alexandrite"></option>
						  <option value="Grade 11 - Quartz"></option>
						  <option value="Grade 11 - Pearl"></option>
						  <option value="Grade 11 - Beryl"></option>
						  <option value="Grade 11 - Amber"></option>
						  <option value="Grade 11 - Neprite"></option>
						  <option value="Grade 11 - Morganite"></option>
						  <option value="Grade 11 - Emerald"></option>
						  <option value="Grade 11 - Diamond"></option>
						  <option value="Grade 11 - Amethyst"></option>
						  <option value="Grade 12 - Kelvin"></option>
						  <option value="Grade 12 - Fahregnheit"></option>
						  <option value="Grade 12 - Celcius"></option>
						  <option value="Grade 12 - Rankine"></option>
						  <option value="Grade 12 - Aurelius"></option>
						  <option value="Grade 12 - Freud"></option>
						  <option value="Grade 12 - Dewey"></option>
						  <option value="Grade 12 - Augustine"></option>
						  <option value="Grade 12 - Hobbes"></option>
						  <option value="Grade 12 - Kant"></option>
						  <option value="Grade 12 - Sartre"></option>
						  <option value="Grade 12 - Aristotle"></option>
						  <option value="Grade 12 - Rand"></option>
						  <option value="Grade 12 - Socrates"></option>
						  <option value="Grade 12 - Lao Tzu"></option>
						  <option value="Grade 12 - Mencius"></option>
						</datalist>
						<div class="invalid-feedback" id="err_section"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Strand</label>
						<select id="strand" name="strand" class="form-control ">
							<option value="" selected>Please select strand</option>
							<option value="TVL">Technical Vocational & Livelihood (TVL)</option>
							<option value="ABM">Accountancy Business and Management (ABM)</option>
							<option value="HUMSS">Humanities and Social Sciences (HUMSS)</option>
							<option value="STEM">Science Technology, Engineering and Mathematics (STEM)</option>
						</select>
						<div class="invalid-feedback" id="err_strand"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Province </label>
						<input type="text" id="province" name="province" placeholder="Enter province here" class="form-control " required>
						<div class="invalid-feedback" id="err_province"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">City </label>
						<input type="text" id="city" name="city" placeholder="Enter city here" class="form-control " required>
						<div class="invalid-feedback" id="err_city"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label class="label">Barangay </label>
						<input type="text" id="barangay" name="barangay" placeholder="Enter barangay here" class="form-control " required>
						<div class="invalid-feedback" id="err_barangay"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Address </label>
						<input type="text" id="address" name="address" placeholder="Enter address here" class="form-control " required>
						<div class="invalid-feedback" id="err_address"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label>Guardian Name </label>
						<input type="text" id="guardian_name" name="guardian_name" placeholder="Enter guradian name here" class="form-control " required>
						<div class="invalid-feedback" id="err_guradian_name"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Email </label>
						<input type="email" id="email" name="email" placeholder="Enter email here" class="form-control " required>
						<div class="invalid-feedback" id="err_email"></div>
					</div>
					<div class="position-relative mb-2 col-md-12">
						<label class="label">Password </label>
						<input type="password" id="password" name="password" placeholder="Enter password here" class="form-control " required>
						<div class="invalid-feedback" id="err_password"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer d-block">
				<div class="row">
					<div class="col-sm-6 col-12">
						<button class="btn btn-dark col-sm-12 col-12" data-bs-dismiss="modal" type="button" id="cancel_btn">Cancel</button>
					</div>
					<div class="col-sm-6 col-12">
						<button class="btn btn-success col-sm-12 col-12" id="btn_submit_user_accounts" type="submit">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
$('#cancel_btn').on('click', function(){
});


$('#refresh-filter').on('click', function(){
	$('#grade_and_section').val('');
	$('#strand_filter').val('');
	submit_filter();

});

function submit_filter(){
	show_user_accounts();
	}
	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){

		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available"
	},
	ajax: {
        url: "{{ route('user_accounts.list') }}",
        data: {filter_grade_section : $('#grade_and_section').val(), filter_strand : $('#strand_filter').val()}
    },
		columns: [{
		className: '',
		"data": "student_no.",
		"title": "Student no.",
		"orderable": false,
	},{
		className: '',
		"data": "first_name",
		"title": "Full Name",
		"orderable": false,
		"render": function(data, type, row, meta){
      return row.last_name+', '+row.first_name
    }
	},{
		className: '',
		"data": "section",
		"title": "Grade and section",
	},{
		className: '',
		"data": "strand",
		"title": "Strand",
		"orderable": false,
	},{
		className: '',
		"data": "gender",
		"title": "Gender",
	},{
		className: '',
		"data": "contact",
		"title": "Contact",
		"orderable": false,
	},{
		className: 'width-option-1 text-center',
    "data": "user_status",
    "title": "Status",
      "render": function(data, type, row, meta){
        newdata = '';
        if (data == 0) {
        newdata+='<div class="btn-group dropup">';
		newdata+='<button type="button" class="btn btn-warning dropdown-toggle btn-sm" style="width: 80px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
		newdata+='Pending';
		newdata+='</button>';
		newdata+='<div class="dropdown-menu bg-secondary">';
		newdata+='<div class="col-sm-12 text-center ">';
		newdata+='<button class="btn btn-sm btn-success" style="width:150px" id="approve_btn" onclick="approave_status('+row.user_id+')">approve</button>';
		newdata+='</div>';
		newdata+='<div class="col-sm-12 text-center mt-1">';
		newdata+='<button class="btn btn-sm btn-danger" style="width:150px" onclick="decline_status('+row.user_id+')">decline</button>';
		newdata+='</div>';
		newdata+='</div>';
		newdata+='</div>';
        }else if(data == 1){
        newdata+='<div class="btn-group dropup">';
		newdata+='<button type="button" class="btn btn-success dropdown-toggle btn-sm" style="width: 80px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
		newdata+='Approved';
		newdata+='</button>';
		newdata+='<div class="dropdown-menu bg-secondary">';
		newdata+='<div class="col-sm-12 text-center ">';
		newdata+='<button class="btn btn-sm btn-success" style="width:150px" id="approve_btn" onclick="approave_status('+row.user_id+')">approve</button>';
		newdata+='</div>';
		newdata+='<div class="col-sm-12 text-center mt-1">';
		newdata+='<button class="btn btn-sm btn-danger" style="width:150px" onclick="decline_status('+row.user_id+')">decline</button>';
		newdata+='</div>';
		newdata+='</div>';
		newdata+='</div>';
        }else{
        newdata+='<div class="btn-group dropup">';
		newdata+='<button type="button" class="btn btn-danger dropdown-toggle btn-sm" style="width: 80px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >';
		newdata+='Declined';
		newdata+='</button>';
		newdata+='<div class="dropdown-menu bg-secondary">';
		newdata+='<div class="col-sm-12 text-center ">';
		newdata+='<button class="btn btn-sm btn-success" style="width:150px" id="approve_btn" onclick="approave_status('+row.user_id+')">approve</button>';
		newdata+='</div>';
		newdata+='<div class="col-sm-12 text-center mt-1">';
		newdata+='<button class="btn btn-sm btn-danger" style="width:150px" onclick="decline_status('+row.user_id+')">decline</button>';
		newdata+='</div>';
		newdata+='</div>';
		newdata+='</div>';
        }
        return newdata;
    }
	},{
		className: 'width-option-1 text-center',
		"data": "user_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-id=\' '+row.user_id+'\' onclick="edit_user_accounts(this)" type="button"><i class="fa fa-edit"></i></button> ';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.user_id+'\' onclick="delete_user_accounts(this)" type="button"><i class="fa fa-trash"></i></button>';
				return newdata;
			}
		}
	]
	});
	}

	function approave_status(id){
		$.ajax({
		    type:"GET",
		    url:"{{ route('user_accounts.approve')}}",
		    data:{user_id : id},
		    dataType:'json',
		    beforeSend:function(){
		    	Swal.fire('Please wait')
				Swal.showLoading()
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	swal('success',response.message,'success');
		        show_user_accounts();
		     }else{
		      console.log(response);
		     }
		     Swal.close();
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

	}

	function decline_status(id){
		$.ajax({
		    type:"GET",
		    url:"{{ route('user_accounts.disapprove')}}",
		    data:{user_id : id},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        show_user_accounts();
		        
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}

	$("#user_account_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit_user_accounts').prop('disabled', true);
					$('#btn_submit_user_accounts').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'user_account_form');
					show_user_accounts();
					$('#modal_user_accounts').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'user_account_form');
				}
				$('#btn_submit_user_accounts').prop('disabled', false);
				$('#btn_submit_user_accounts').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user_accounts(_this){
		var id = $(_this).attr('data-id');
		var url =  main_path + 'user_accounts/delete/' + id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user_accounts?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_user_accounts();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_user_accounts(){
		$('#password-div').show();

		$('#modal_title_user_accounts').text('Add User accounts');
		$('#user_id').val('');
		showValidator([],'user_account_form');
		$('#user_account_form').removeClass('was-validated');
		$('#user_id').val('');
		$('#first_name').val('');
		$('#middle_name').val('');
		$('#last_name').val('');
		$('#gender').val('');
		$('#contact').val('');
		$('#province').val('');
		$('#city').val('');
		$('#brgy').val('');
		$('#address').val('');
		$('#email').val('');
		$('#password').val('');
		$('#strand').val('');
		$('#section').val('');
		$('#grade').val('');
		$('#barangay').val('');
		$('#student_no').val('');
		$('#modal_user_accounts').modal('show');
	}

	function edit_user_accounts(_this){
		$('#modal_title_user_accounts').text('Edit User accounts');
		var id = $(_this).attr('data-id');
		var url =  main_path + 'user_accounts/find/' + id;
		$.ajax({
		type:"get",
		url:url,
		data:{},
		dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			let data = response.data;
			$('#user_id').val(data.user_id);
			$('#first_name').val(data.first_name);
			$('#middle_name').val(data.middle_name);
			$('#last_name').val(data.last_name);
			$('#contact').val(data.contact);
			$('#province').val(data.province);
			$('#city').val(data.city);
			$('#brgy').val(data.brgy);
			$('#address').val(data.address);
			$('#email').val(data.email);
			$('#student_no').val(data.student_no);
			$('#strand').val(data.strand);
			$('#grade').val(data.grade);
			$('#gender').val(data.gender);
			$('#strand').val(data.strand);
			$('#section').val(data.section);
			$('#grade').val(data.grade);
			$('#guardian_name').val(data.guardian_name);
			$('#barangay').val(data.barangay);
			$('#modal_user_accounts').modal('show');
		},
		error: function(error){
			console.log(error);
		}
		});
	}
</script>
@endsection
