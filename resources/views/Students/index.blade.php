@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
<div class="row pt-3">
	<div class="col-lg-12 col-md-12 col-12">
		<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
			<div class="mb-3 mb-md-0">
				<h1 class="mb-1 h2 fw-bold">Students</h1>
			</div>
			<div class="d-flex">
				  <button class="btn btn-primary btn-shadow" onclick="add_students();"><i class="fa fa-plus"></i> Add Students</button>
			</div>
		</div>
	</div>
</div>

<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered dt-responsive nowrap" id="tbl_students" style="width: 100%;"></table>
		</div>
	</div>
</div>

<form class="needs-validation" id="student_form" action="{{ url('students/add') }}" novalidate>
	<div class="modal fade" role="dialog" id="modal_students" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="modal_title_students">
					Add students
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="student_id" name="student_id" placeholder="" class="form-control" required>
					<div class="position-relative mb-2 col-md-12">
						<label>Firstname </label>
						<input type="text" id="firstname" name="firstname" placeholder="Enter Lastname" class="form-control " required>
						<div class="invalid-feedback" id="err_firstname"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Middlename </label>
						<input type="text" id="middlename" name="middlename" placeholder="Enter Middlename" class="form-control " required>
						<div class="invalid-feedback" id="err_middlename"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Lastname </label>
						<input type="text" id="lastname" name="lastname" placeholder="Enter Lastname" class="form-control " required>
						<div class="invalid-feedback" id="err_lastname"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Birthdate </label>
						<input type="date" id="birthdate" name="birthdate" placeholder="Enter Birthdate" class="form-control " required>
						<div class="invalid-feedback" id="err_birthdate"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Gender </label>
						<select id="gender" name="gender" class="form-control ">
							<option value="" selected="" disabled="">Please selelct Gender</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
						<div class="position-relative mb-2 col-md-12">
						<label>Address </label>
						<input type="text" id="address" name="address" placeholder="Enter your address" class="form-control " required>
						<div class="invalid-feedback" id="err_address"></div>
					</div>
					<div class="position-relative mb-2 col-md-6">
						<label>Grade </label>
						<select id="grade" name="grade" class="form-control ">
							<option value="" selected="" disabled="">Grade level</option>
							<option value="grade 10">Grade 10</option>
							<option value="grade 11">Grade 11</option>
						</select>
						<div class="invalid-feedback" id="err_grade"></div>
					</div>
						<div class="position-relative mb-2 col-md-6">
						<label>Section </label>
						<select id="section" name="section" class="form-control ">
							<option value="" selected="" disabled="">Student Section</option>
							<option value="Albert Einstein">Albert Einstein</option>
							<option value="Nikola tesla">Nikola tesla</option>
						</select>
						<div class="invalid-feedback" id="err_section"></div>
					</div>
					</div>
			</div>
			<div class="modal-footer d-block">
				<div class="row">
					<div class="col-sm-6 col-12">
						<button class="btn btn-dark col-sm-12 col-12" data-dismiss="modal" type="button">Cancel</button>
					</div>
					<div class="col-sm-6 col-12">
						<button class="btn btn-success col-sm-12 col-12" id="btn_submit_students" type="submit">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
	show_students();
	var tbl_students;
	function show_students(){
		if (tbl_students) {
			tbl_students.destroy();
		}
		var url = main_path + 'students/list';
		tbl_students = $('#tbl_students').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "firstname",
		"title": "Firstname",
	},{
		className: '',
		"data": "middlename",
		"title": "Middlename",
	},{
		className: '',
		"data": "lastname",
		"title": "Lastname",
	},{
		className: '',
		"data": "birthdate",
		"title": "Birthdate",
	},{
		className: '',
		"data": "gender",
		"title": "Gender",
	},{
		className: '',
		"data": "address",
		"title": "Address",
	},{
		className: '',
		"data": "grade",
		"title": "Grade",
	},{
		className: '',
		"data": "section",
		"title": "Section",
	},{
		className: 'width-option-1 text-center',
		"data": "student_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-id=\' '+row.student_id+'\' onclick="edit_students(this)" type="button"><i class="fa fa-edit"></i></button> ';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.student_id+'\' onclick="delete_students(this)" type="button"><i class="fa fa-trash"></i></button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#student_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit_students').prop('disabled', true);
					$('#btn_submit_students').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'student_form');
					show_students();
					$('#modal_students').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'student_form');
				}
				$('#btn_submit_students').prop('disabled', false);
				$('#btn_submit_students').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_students(_this){
		var id = $(_this).attr('data-id');
		var url =  main_path + 'students/delete/' + id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this students?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_students();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_students(){
		$('#modal_title_students').text('Add Students');
		$('#student_id').val('');
		showValidator([],'student_form');
		$('#student_form').removeClass('was-validated');
		$('#firstname').val('');
		$('#middlename').val('');
		$('#lastname').val('');
		$('#birthdate').val('');
		$('#gender').val('');
		$('#address').val('');
		$('#grade').val('');
		$('#section').val('');
		$('#modal_students').modal('show');
	}

	function edit_students(_this){
		$('#modal_title_students').text('Edit Students');
		var id = $(_this).attr('data-id');
		var url =  main_path + 'students/find/' + id;
		$.ajax({
		type:"get",
		url:url,
		data:{},
		dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			let data = response.data;
			$('#student_id').val(data.student_id);
			$('#firstname').val(data.firstname);
			$('#middlename').val(data.middlename);
			$('#lastname').val(data.lastname);
			$('#birthdate').val(data.birthdate);
			$('#gender').val(data.gender);
			$('#address').val(data.address);
			$('#grade').val(data.grade);
			$('#section').val(data.section);
			$('#modal_students').modal('show');
		},
		error: function(error){
			console.log(error);
		}
		});
	}
</script>
@endsection
