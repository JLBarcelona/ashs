<aside class="main-sidebar sidebar-dark-dark bg-dark elevation-4">
  <!-- Brand Logo -->
   <a class="brand-link" style="border-color: #a4b0d6;">
      <img src="{{ asset('img/logo.png') }}" alt="Miakata Logo" class="brand-image" >
      <span class="brand-text font-weight-light">ASHS</span>
    </a>
  
  <!-- Sidebar -->
  <div class="sidebar p-0 mt-2">
    <!-- Sidebar user panel (optional) -->
    <!-- Sidebar Menu -->
    <nav class="sidebar side-item">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <!-- Sample Multi level -->
        <li class="nav-item ml-4 mt-1">
            <p class="text-primary text-lg"> 
              <i class="fa-sharp fa-solid fa-user"> </i>
              {{' '.Auth::user()->first_name.' '.Auth::user()->last_name}}
            </p>
        </li>
        <li class="nav-item ">
          <a href="{{ route('admin.index')}}" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('problem-dashboard.index')}}" class="nav-link">
            <i class="nav-icon fas fa-hands-helping"></i>
            <p>
              Counseling Request
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('problem-dashboard.active')}}" class="nav-link">
            <i class="nav-icon fas fa-envelope"></i>
            <p>
              Active Counseling
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('problem-dashboard.solve')}}" class="nav-link">
            <i class="nav-icon fas fa-envelope"></i>
            <p>
              Solved Counseling
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('user_accounts.index')}}" class="nav-link">
            <i class="nav-icon fas fa-user-graduate"></i>
            <p>
              Student Management
            </p>
          </a>
        </li>
       <!--  <li class="nav-item ">
          <a href="{{ route('staff.index')}}" class="nav-link">
            <i class="nav-icon fas fa-user-cog"></i>
            <p>
              Staff Management
            </p>
          </a>
        </li> -->
        <li class="nav-item ">
          <a href="{{ route('news_and_update.index')}}" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
              News and Updates
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
