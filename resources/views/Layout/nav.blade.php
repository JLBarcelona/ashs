<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
    <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" class="img-fluid" width="30" alt=""> AURORA SENIOR HIGH SCHOOL</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      </li>
      <li class="nav-item">
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <a  href="{{ route('login') }}" class="btn btn-primary my-2 my-sm-0" type="submit">Login</a>
      <a  href="{{ route('registration') }}" class="btn btn-success my-2 my-sm-0 ml-2" type="submit">Signup</a>
    </div>
  </div>
</nav>