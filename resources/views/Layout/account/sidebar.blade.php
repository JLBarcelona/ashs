<aside class="main-sidebar sidebar-dark-dark bg-dark elevation-4">
  <!-- Brand Logo -->
   <a class="brand-link" style="border-color: #a4b0d6;">
      <img src="{{ asset('img/logo.png') }}" alt="Miakata Logo" class="brand-image" >
      <span class="brand-text font-weight-light">ASHS</span>
    </a>
  <div class="sidebar p-0 mt-2">
    <nav class="sidebar side-item">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item ml-4 mt-1">
            <p class="text-primary text-lg"> 
              <i class="fa-sharp fa-solid fa-user"> </i>
              {{' '.Auth::user()->first_name.' '.Auth::user()->last_name}}
            </p>
        </li>
        <!-- <li class="nav-item ">
          <a href="{{ route('account.index') }}" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> 
            <p>
              Dashboard
            </p>
          </a>
        </li> -->
        <li class="nav-item ">
          <a href="{{ route('problems.index')}}" class="nav-link">
            <i class="fas fa-sms"></i>
            <p>
              Counselling
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('account-inbox.index') }}" class="nav-link">
            <i class="fas fa-inbox"></i>
            <p>
              Inbox
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('student-profile.index')}}" class="nav-link">
            <i class="fas fa-user-cog"></i>
            <p>
              Profile Settings
            </p>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>
