<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition sidebar-mini layout-fixed">
		<div class="wrapper">
			@include('Layout.account.nav')
			@include('Layout.account.sidebar')
		 	@yield('breadcrumbs')
		 	<div class="content-wrapper">
		 		<section class="content">
					 <div class="container-fluid">
							@yield('content')
					</div>
				 </section>
		 	</div>
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>