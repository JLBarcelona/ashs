<nav class="main-header navbar navbar-expand-lg bg-dark navbar-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav">
      <li class="nav-item">
         <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <ul class="navbar-nav">
      <li class="nav-item">
         <a class="nav-link" href="{{ route('home') }}">News and Updates</a>
      </li>
    </ul>

    <!-- <ul class="navbar-nav ml-auto pr-2">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          @if(count_all_notifs() == 0)
          @else
          <span class="badge badge-warning navbar-badge">{{count_all_notifs()}}</span>
          @endif
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">{{count_all_notifs()}} Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item text-primary">
            <i class="nav-icon fas fa-hands-helping"></i> {{notif_problem()}} new counseling
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item text-primary">
           <i class="fas fa-envelope mr-2"></i> unread message
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item text-primary">
             <i class="fas fa-users mr-2"></i> {{new_students()}} pending students
          </a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul> -->

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
         <a  href="{{ route('logout') }}" class="btn btn-outline-secondary my-2 my-sm-0 text-light">Logout</a>
      </li>
    </ul>
  </div>
</nav>

<!--  -->