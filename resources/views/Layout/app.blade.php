<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition">
		<div class="">
			@include('Layout.nav', ['is_disabled' => (isset($is_disabled) && $is_disabled == true)? $is_disabled : false])
			 	@yield('breadcrumbs')
			 	 <section class="content">
						@yield('content')
				 </section>
			</div>
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>