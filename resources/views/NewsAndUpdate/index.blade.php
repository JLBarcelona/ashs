@extends('Layout.admin_app')
@section('title', 'News-and-updates')
@section('content')
  <div class="container-fluid pt-4">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header h4"><b>News and Update</b></div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                @foreach($news as $new)
                <form action="#" id="news_and_update_form">
                <label for="news">Latest Activities</label>
                <textarea class="form-control" rows="5" placeholder="Enter activities here" oninput="activities_input(this.value)" id="activities" readonly style="font-size: 25px">{{ $new->activities}}</textarea>
              </div>
              <div class="col-sm-12 mt-4">
                <label for="news">News and Update</label>
                <textarea class="form-control" rows="5" placeholder="Enter news and update here" oninput="news_input(this.value)" id="news" readonly style="font-size: 25px">{{ $new->news}}</textarea>
              </div>
              @endforeach
            </div>
          </div>
          <div class="card-footer text-right">
           <label id="note" class="mr-3">Press edit button to edit</label>
            <button type="button" class="px-4 btn btn-primary" onclick="edit()">Edit</button>
            <button type="submit" class="px-4 btn btn-success" onclick="Save()">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">

  function Save(){
    $('#activities').attr('readonly', true);
    $('#news').attr('readonly', true);
    $('#note').html('Press edit button to edit');
  }

  function edit(){
    $('#activities').attr('readonly', false);
    $('#news').attr('readonly', false);
    $('#note').html('Press save button to save');


  }

  function news_input(_this){
    $.ajax({
        type:"GET",
        url:"{{ route('news_and_update.news') }}",
        data:{this : _this},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  function activities_input(_this){
    $.ajax({
        type:"GET",
        url:"{{ route('news_and_update.activities') }}",
        data:{this : _this},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  }
  
  function clear_btn(){
  $('#news_and_update_form')[0].reset();
  }
</script>
@endsection
