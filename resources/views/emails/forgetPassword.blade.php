@component('mail::message')
<label>Hello, {{ $data['name'] }}!</label><br>
<label>We are here to help you recover your account</label><br>
<label>Click the button below to change your password</label><br>
<a href="https://ashs.tech/reset-password/{{ $data['user_id'] }}">
<button style="border-radius: 5px; background-color: #4169E1; color: white; border: none; height: 30px">Change Password</button></a>
Thanks, Aurora Senior High School<br>
@endcomponent