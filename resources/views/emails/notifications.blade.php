@component('mail::message')
<label>Hello, {{ $data['name'] }}!</label><br>
<label>Your application to join the ASHS website has been accepted.</label><br>
<label>The button will take you to our website.</label><br>
<a href="https://ashs.tech/">
<button style="border-radius: 5px; background-color: #4169E1; color: white; border: none; height: 30px;">ASHS Website</button>
</a><br>
Thanks, Aurora Senior High School<br>
@endcomponent