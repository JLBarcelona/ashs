@extends('Layout.app')
@section('title', 'ASHS Official Website')
@section('css')
<style type="text/css">
	.labels:hover{
		color: orange;
	}
</style>
@endsection
@section('content')
<body style="background-image: url('public/img/schoolbook.jpg');background-position: center; background-repeat: no-repeat; background-size: cover;">

  <nav class="navbar navbar-expand-lg bg-secondary navbar-secondary justify-content-center pb-0">
    <a class="navbar-brand mx-5 " href="{{ route('aboutus') }}" ><p class="labels {{ (request()->is('aboutus'))? 'text-warning': '' }}">About us</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('Organization') }}"><p class="labels {{ (request()->is('Organization'))? 'text-warning': '' }}">Organization</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('activities')}}"><p class="labels {{ (request()->is('activities'))? 'text-warning': '' }}">Activities</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('newsupdates') }}"><p class="labels {{ (request()->is('newsupdates'))? 'text-warning': '' }}">News and Updates</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('strands') }}"><p class="labels {{ (request()->is('strands'))? 'text-warning': '' }}">Strands</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('visionMission') }}"><p class="labels {{ (request()->is('visionMission'))? 'text-warning': '' }}">Vision/Mission</p></a>
  </nav>

  <div class="text-warning pt-3 text-center">
  <img src="{{ asset('img/logo.png') }}" class="img-fluid pt-0" style="width: 25vh">
  <h1> <strong>About Us</strong></h1>
  <div>
 <p class="h3"> Ready to Listen, Ready to Learn Together. Catering for students of Aurora Senior High School as</p>
 <p class="h3">  well as the school website, student profiling, and guidance services system is a partnership that is </p>
 <p class="h3">a collaboration of consultation and counseling. The various website provides support and information  </p>
 <p class="h3">to the user quickly and efficiently. </p>
  </div>
  </div>
</body>
@endsection

