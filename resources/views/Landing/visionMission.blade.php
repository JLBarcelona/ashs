@extends('Layout.app')
@section('title', 'ASHS Official Website')
@section('css')
<style type="text/css">
	.labels:hover{
		color: orange;
	}
</style>
@endsection
@section('content')
<body style="background-image: url('public/img/schoolbook.jpg');background-position: center; background-repeat: no-repeat; background-size: cover;">

  <nav class="navbar navbar-expand-lg bg-secondary navbar-secondary justify-content-center pb-0">
    <a class="navbar-brand mx-5 " href="{{ route('aboutus') }}" ><p class="labels {{ (request()->is('aboutus'))? 'text-warning': '' }}">About us</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('Organization') }}"><p class="labels {{ (request()->is('Organization'))? 'text-warning': '' }}">Organization</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('activities')}}"><p class="labels {{ (request()->is('activities'))? 'text-warning': '' }}">Activities</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('newsupdates') }}"><p class="labels {{ (request()->is('newsupdates'))? 'text-warning': '' }}">News and Updates</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('strands') }}"><p class="labels {{ (request()->is('strands'))? 'text-warning': '' }}">Strands</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('visionMission') }}"><p class="labels {{ (request()->is('visionMission'))? 'text-warning': '' }}">Vision/Mission</p></a>
  </nav>

 <div class="container-fluid mt-5">
  <div class="row justify-content-center">
  	<div class="col-sm-4 text-center text-light">
  		<h1><strong>vision</strong></h1>
  		<img src="{{ asset('img/21 (3).jpg') }}" class="img-fluid" style="height: 60vh; border-radius: 25px">

  	</div>

  	<div class="col-sm-4 text-center text-light">
  		<h1><strong>Mission</strong></h1>
  		<img src="{{ asset('img/21 (2).jpg') }}" class="img-fluid" style="height: 60vh; border-radius: 25px">
  	</div>

  	<div class="col-sm-4 text-center text-light">
  		<h1><strong>Core Values</strong></h1>
  		<img src="{{ asset('img/21 (1).jpg') }}" class="img-fluid" style="height: 60vh; border-radius: 25px">


  	</div>
  </div>
 </div>
</body>
@endsection

