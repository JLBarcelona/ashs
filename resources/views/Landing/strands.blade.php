@extends('Layout.app')
@section('title', 'ASHS Official Website')
@section('css')
<style type="text/css">
	.labels:hover{
		color: orange;
	}
</style>
@endsection
@section('content')
<body style="background-image: url('public/img/schoolbook.jpg');background-position: center; background-repeat: no-repeat; background-size: cover;">

  <nav class="navbar navbar-expand-lg bg-secondary navbar-secondary justify-content-center pb-0">
    <a class="navbar-brand mx-5 " href="{{ route('aboutus') }}" ><p class="labels {{ (request()->is('aboutus'))? 'text-warning': '' }}">About us</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('Organization') }}"><p class="labels {{ (request()->is('Organization'))? 'text-warning': '' }}">Organization</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('activities')}}"><p class="labels {{ (request()->is('activities'))? 'text-warning': '' }}">Activities</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('newsupdates') }}"><p class="labels {{ (request()->is('newsupdates'))? 'text-warning': '' }}">News and Updates</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('strands') }}"><p class="labels {{ (request()->is('strands'))? 'text-warning': '' }}">Strands</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('visionMission') }}"><p class="labels {{ (request()->is('visionMission'))? 'text-warning': '' }}">Vision/Mission</p></a>
  </nav>


<div class="text-warning pt-3 text-center">
  <img src="{{ asset('img/logo.png') }}" class="img-fluid pt-0" style="width: 25vh">
  <h1> <strong>Strands</strong></h1>
  <div>
 <p class="h2">Science Technology, Engineering and Mathematics (STEM)</p>
 <p class="h2">Accountancy Business and Management (ABM)</p>
 <p class="h2">Humanities and Social Sciences (HUMSS)</p>
 <p class="h2">Technical Vocational & Livelihood (TVL)</p>
  </div>
  </div>


</body>
@endsection

