@extends('Layout.app')
@section('title', 'ASHS Official Website')
@section('css')
<style type="text/css">
	.labels:hover{
		color: orange;
	}
    .card-home{
        font-size: 4vh;
        border-color: white;
        height: 69vh;
        color: white;
        width: 75vh;
        padding: 5vh;
        background-color: rgba(0,0,0,0.5) !important;
        box-shadow: -2px 2px 2px 2px rgba(255, 255, 255, 0.1);
    }
    
</style>
@endsection
@section('content')
<body style="background-image: url('public/img/schoolbook.jpg');background-position: center; background-repeat: no-repeat; background-size: cover;">

  <nav class="navbar navbar-expand-lg bg-secondary navbar-secondary justify-content-center pb-0">
    <a class="navbar-brand mx-5 " href="{{ route('aboutus') }}" ><p class="labels {{ (request()->is('aboutus'))? 'text-warning': '' }}">About us</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('Organization') }}"><p class="labels {{ (request()->is('Organization'))? 'text-warning': '' }}">Organization</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('activities')}}"><p class="labels {{ (request()->is('activities'))? 'text-warning': '' }}">Activities</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('newsupdates') }}"><p class="labels {{ (request()->is('newsupdates'))? 'text-warning': '' }}">News and Updates</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('strands') }}"><p class="labels {{ (request()->is('strands'))? 'text-warning': '' }}">Strands</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('visionMission') }}"><p class="labels {{ (request()->is('visionMission'))? 'text-warning': '' }}">Vision/Mission</p></a>
  </nav>
</body>




<div class="container-fluid mt-4">
    <div class="row justify-content-center">
        <div class="col-sm-5 mx-5">
            <h1 class="text-warning text-center"><strong>News</strong> </h1>
            <div class="card card-home">
                <div class="card-body">
                    {!! nl2br(news()) !!}
                </div>
            </div>
        </div>

        <div class="col-sm-5 mx-5">
            <h1 class="text-warning text-center"><strong>Activities</strong> </h1>
             <div class="card card-home">
                <div class="card-body">
                    {!! nl2br(activities()) !!}
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

