@extends('Layout.app')
@section('title', 'ASHS Official Website')
@section('css')
<style type="text/css">
	.labels:hover{
		color: orange;
	}
	.image_active{
		width: 110vh;
		height: 70vh;
	}
</style>
@endsection
@section('content')
<body style="background-image: url('public/img/schoolbook.jpg');background-position: center; background-repeat: no-repeat; background-size: cover;">

  <nav class="navbar navbar-expand-lg bg-secondary navbar-secondary justify-content-center pb-0">
    <a class="navbar-brand mx-5 " href="{{ route('aboutus') }}" ><p class="labels {{ (request()->is('aboutus'))? 'text-warning': '' }}">About us</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('Organization') }}"><p class="labels {{ (request()->is('Organization'))? 'text-warning': '' }}">Organization</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('activities')}}"><p class="labels {{ (request()->is('activities'))? 'text-warning': '' }}">Activities</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('newsupdates') }}"><p class="labels {{ (request()->is('newsupdates'))? 'text-warning': '' }}">News and Updates</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('strands') }}"><p class="labels {{ (request()->is('strands'))? 'text-warning': '' }}">Strands</p></a>
    <a class="navbar-brand mx-5 " href="{{ route('visionMission') }}"><p class="labels {{ (request()->is('visionMission'))? 'text-warning': '' }}">Vision/Mission</p></a>
  </nav>


<div id="carouselExampleIndicators" class="carousel slide" data-ride="true">
  <div class="carousel-indicators">
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="1" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="2" aria-label="Slide 2"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="3" aria-label="Slide 3"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="4" aria-label="Slide 4"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="5" aria-label="Slide 5"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="6" aria-label="Slide 6"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="7" aria-label="Slide 7"></button>
    <button type="button" data-target="#carouselExampleIndicators" data-slide-to="8" aria-label="Slide 8"></button>
  </div>
  <div class="carousel-inner text-center mt-5">
    <div class="carousel-item active">
      <img src="{{ asset('img/activities (1).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img/activities (2).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img/activities (3).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
 	<div class="carousel-item">
      <img src="{{ asset('img/activities (4).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
   
    <div class="carousel-item">
      <img src="{{ asset('img/activities (5).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
  	<div class="carousel-item">
      <img src="{{ asset('img/activities (6).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
   
    <div class="carousel-item">
      <img src="{{ asset('img/activities (7).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
  	<div class="carousel-item">
      <img src="{{ asset('img/activities (8).jpg')}}" class="img-fluid img-thumbnail image_active">
    </div>
  <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
</body>
@endsection

