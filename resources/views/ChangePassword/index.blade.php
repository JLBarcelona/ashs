@extends('Layout.account.app')
@section('title', 'Change-Password')
 <style type="text/css">
    .input-group-text.bg-secondary{
      width: 145px;
    }
  </style>
@section('content')
    <form action="{{ route('student-profile.change') }}" novalidate="" id="account_edit_form" class="needs-validation">
      <div class="container-fluid pt-5">
        <div class="row justify-content-center">
          <div class="col-sm-12">
          <div class="card">
            <div class="card-header text-primary h5">Change Password</div>
            <div class="card-body">
              
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary" style="">Old Password</span>
                </div>
                <input type="password" id="old_pass" name="old_pass" class="form-control" placeholder="Enter old password " aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                  <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye1', 'old_pass');"><i class="fa-solid fa-eye" id="eye1"></i></span>
                </div>
                <div class="invalid-feedback" id="err_old_pass"></div>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary" style="">New Password</span>
                </div>
                <input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="Enter new password " aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                  <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye2', 'new_pass');"><i class="fa-solid fa-eye" id="eye2"></i></span>
                </div>
                <div class="invalid-feedback" id="err_new_pass"></div>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary" style="">Confirm Password</span>
                </div>
                <input type="password" id="confirm_pass" name="confirm_pass" class="form-control" placeholder="Confirm password " aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                  <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye3', 'confirm_pass');"><i class="fa-solid fa-eye" id="eye3"></i></span>
                </div>
                <div class="invalid-feedback" id="err_confirm_pass"></div>
              </div>
            </div>  
            <div class="card-footer text-right">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </div>
        </div>
        </div>
        </div>
      </div>
    </div>
  </form>
@endsection
@section('script')
<script type="text/javascript">
	 function password_visibile(icon, input){
    $("#"+icon).toggleClass('fa-eye fa-eye-slash');
    $("#"+input).attr('type', function(index, attr){
      return (attr == 'text')? 'password' : 'text';
    });
  }

  function clear(){
   
    $('#old_pass').val('');
    $('#new_pass').val('');
    $('#confirm_pass').val('');
  }


  $('#account_edit_form').on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let formData = $(this).serialize();
    $.ajax({
        type:"get",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status === true) {
            swal("Success", response.message, "success");
            clear();
         }else{
          console.log(response);
            swal("error", response.message, "error");
          showValidator(response.error, 'account_edit_form');

         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });

</script>
@endsection
