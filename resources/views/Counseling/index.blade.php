@extends('Layout.admin_app')
@section('title', 'Counseling')
@section('content')
<div class="container-fluid pt-4">
  <div class="row justify-content-center">
    <div class="col-sm-6">
        <div class="card direct-chat direct-chat-primary">
          <div class="card-header">
            <h3 class="card-title">Student Name</h3>
          </div>
          <div class="card-body">
            <div class="direct-chat-messages">
              <div class="direct-chat-msg">
                <div class="direct-chat-infos clearfix">
                  <span class="direct-chat-name float-left">Alexander Pierce</span>
                  <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
                </div>
                <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                <div class="direct-chat-text">
                  Is this template really for free? That's unbelievable!
                </div>
              </div>
              <div class="direct-chat-msg right">
                <div class="direct-chat-infos clearfix">
                  <span class="direct-chat-name float-right">Sarah Bullock</span>
                  <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                </div>
                <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                <div class="direct-chat-text">
                  You better believe it!
                </div>
              </div>
              <div class="direct-chat-msg">
                <div class="direct-chat-infos clearfix">
                  <span class="direct-chat-name float-left">Alexander Pierce</span>
                  <span class="direct-chat-timestamp float-right">23 Jan 5:37 pm</span>
                </div>
                <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                <div class="direct-chat-text">
                  Working with AdminLTE on a great new app! Wanna join?
                </div>
              </div>
              <div class="direct-chat-msg right">
                <div class="direct-chat-infos clearfix">
                  <span class="direct-chat-name float-right">Sarah Bullock</span>
                  <span class="direct-chat-timestamp float-left">23 Jan 6:10 pm</span>
                </div>
                <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                <div class="direct-chat-text">
                  I would love to.
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <form action="#" method="post">
              <div class="input-group">
                <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                <span class="input-group-append">
                  <button type="button" class="btn btn-primary">Send</button>
                </span>
              </div>
            </form>
          </div>
        </div>
    </div>
  </div>  
</div>
@endsection

@section('script')
<script type="text/javascript">
  show_Users();
  var table_problem_id;
  function show_Users(){
    if (table_problem_id) {
      table_problem_id.destroy();
    }
    
    table_problem_id = $('#table_problem_id').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: "{{ route('problem-dashboard.list')}}",
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "users.first_name",
    "title": "Firstname",
  },{
    className: '',
    "data": "users.last_name",
    "title": "Lastname",
  },{
    className: '',
    "data": "problem_type",
    "title": "Problem Type",
  },{
    className: '',
    "data": "problem_message",
    "title": "Problem Mesage",
  },{
    className: '',
    "data": "date",
    "title": "Date",
  }
  ,{
    className: 'width-option-1 text-center',
    "data": "status",
    "orderable": false,
    "title": "Status",
      "render": function(data, type, row, meta){
        newdata = '';
        if (data == 0) {
        newdata += ' <span class="badge badge-warning mt-1" onclick="delete_Users()" style="width: 80px" type="button">Pending</i></span>';
        }else if(data == 1){
        newdata += ' <span class="badge badge-success mt-1" onclick="delete_Users()" style="width: 80px" type="button">Approved</i></span>';
        }else{
        newdata += ' <span class="badge badge-danger mt-1" onclick="delete_Users()" style="width: 80px" type="button">Declined</i></span>';

        }
        return newdata;
    }
    },
    {
    className: 'width-option-1 text-center',
    "data": "user_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        newdata = '';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="approve_disapprove('+row.id+', \'decline\')" type="button" title="Talk to Student"><i class="fa fa-envelope"></i></button>';
        return newdata;
      }
    }
  ]
  });
  }
 
</script>
@endsection
