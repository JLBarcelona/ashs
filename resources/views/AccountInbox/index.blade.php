@extends('Layout.account.app')
@section('title', 'Inbox')
@section('css')
<style type="text/css">
	.selector:hover {
		background-color: #DCDCD2;
		border-radius: 10px;
		cursor: pointer;
	}
	.scroll {
    max-height: 90vh;
    overflow-y: auto;
}
</style>
@endsection
@section('content')
<div class="container-fluid py-4">
	<div class="row">
			<div class="col-sm-12">
				
				<div class="card">
					<div class="card-header text-xl">
						<label>Chats</label>
					</div>
					<div class="card-body scroll" style="height: 69vh">
						@if(count($problem) < 1)
						<h5><strong>No Message Found.</strong> </h5>
						@else
							@foreach($problem as $problem)
								<div class="col-sm-12 selector" onclick="go_forum('{{ $problem->id }}')">
									<label class="text-lg" style="cursor: pointer;">Problem Type: <span class="text-danger">{{ $problem->problem_type}}</span></label>
									@if($problem->forums_is_notseen_count == 0)
									<label style="cursor: pointer;" class="float-sm-right text-primary">No new message</i>
									</label>
									@else
									<label style="cursor: pointer;" class="float-sm-right text-primary">{{$problem->forums_is_notseen_count}} new message</i>
									</label>
									@endif
									<br>
									@if($problem->status == 1)
									<label style="cursor: pointer;" class=""> Status: <span class="text-primary mr-2">Active</span> </i></label>

									@else
									<label style="cursor: pointer;" class=""> Status: <span class="text-success mr-2">Solve</span> </i></label>

									@endif
									<label>|</label>
									<label style="cursor: pointer;">{{ date('M-d-Y', strtotime($problem->created_at)) }}</label>
									<hr>
								</div>
							@endforeach
						@endif
					</div>
						<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
<script type="text/javascript">
	function go_forum(id){
    window.location.href = "{{ route('account-forum.index') }}"+'/'+id;
    $.ajax({
        type:"GET",
        url:"{{ route('account-inbox.seen') }}"+'/'+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            // swal("Success", response.message, "success");
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  }

</script>
@endsection
