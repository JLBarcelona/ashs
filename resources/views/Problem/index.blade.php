@extends('Layout.account.app')
@section('title', 'Counseling')
@section('content')
	<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Problems Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_problems();" class="btn btn-primary">Add Problems</button></div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_problems" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="problem_form" action="{{ route('problems.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_problem_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_problem_form_title">Add Problems</h5>
					<button class="close" data-dismiss="modal" onclick="close_modal()">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12 mb-2 mt-2">
							<label>Problem type</label>
							<select name="problem_type" id="problem_type" class="form-control" onchange="problem_select(this.value)">
								<option value="" selected="" disabled="">Please Select Problem type</option>
								<option value="Family Problem">Family Problem</option>
								<option value="Financial Problem">Financial Problem</option>
								<option value="Academics Problem">Academics Problem</option>
								<option value="Other">Other</option>
							</select>
							<div class="invalid-feedback" id="err_problem_type"></div>
						</div>
						<div class="col-md-12 mb-2 mt-2" id="other_problem_div">
							<label>Other Problem</label>
							<input type="text" name="other_option" id="other_option" class="form-control " required  placeholder="Specify the problem"/>
							<div class="invalid-feedback" id="err_other_option"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="problem_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('script')
<script type="text/javascript">

function problem_select(val){
	if ($('#problem_type').val() == 'Other') {
	$('#other_problem_div').show();

	}else{
	$('#other_problem_div').hide();
	$('#other_option').val('');

	}
}

function add_problems(){
	$("#id").val('');
	$('#problem_type').val('');
	$('#problem_message').val('');
	$('#other_problem_div').hide();
	$("#modal_problem_form").modal('show');
}

var tbl_problems;
show_problems();
function show_problems(){
	if(tbl_problems){
		tbl_problems.destroy();
	}
	tbl_samples = $('#tbl_problems').DataTable({
		destroy: true,
		pageLength: 10,
		responsive: true,
		aaSorting: [],
		ajax: "{{ route('problems.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'problem_type',
				"title": 'Problem type',
				"orderable": false,
			},
			{
				className: '',
				"data": 'date',
				"title": 'Date',
				"orderable": false,
			},
			{
				className: '',
				"data": 'status',
				"title": 'Status',
				"class": 'text-center',
				"orderable": false,
				"render": function(data, type, row, meta){
		        newdata = '';
		        if (data == 0) {
		        newdata += ' <span class="badge badge-warning mt-1" style="width: 80px" type="button">Pending</i></span>';
		        }else if(data == 1){
		        newdata += ' <span class="badge badge-primary mt-1" style="width: 80px" type="button">Approved</i></span>';
		        }else if(data == 2){
		        newdata += ' <span class="badge badge-danger mt-1" style="width: 80px" type="button">Declined</i></span>';
		        }else{
		        newdata += ' <span class="badge badge-success mt-1" style="width: 80px" type="button">Solved</i></span>';
		        }
		        return newdata;
		    }
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_problems('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_problems('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					return newdata;
				}
			}
		]
	});
}


$("#problem_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			
		},
		success:function(response){
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_problems();
				$('#modal_problem_form').modal('hide');
			}else{
				showValidator(response.error,'user_account_form');
				console.log(response);
			}
				validation('problem_form', response.error);
		},
		error: function(error){
			
			console.log(error);
		}
	});
});

function edit_problems(id){
	$.ajax({
		type:"GET",
		url:"{{ route('problems.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			if (response.status == true) {
				$('#other_problem_div').hide();
				$('#id').val(response.data.id);
				$('#problem_type').val(response.data.problem_type);
				$('#problem_message').val(response.data.problem_message);
				$('#modal_problem_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_problems(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete problems?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('problems.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_problems();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

</script>
@endsection
