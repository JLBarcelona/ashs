<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Forum;
use App\Models\Problem;
use Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class DashboardController extends Controller
{
	public function index(){
		$pending = Problem::where('status', 0)->count();
		$active = Problem::where('status', 1)->count();
		$solve = Problem::where('status', 3)->count();
		$students = User::whereNull('deleted_at')->where('user_type', 3)->count();
		$staff = User::whereNull('deleted_at')->where('user_type', 2)->count();

		// return get_student_per_section()['data'];
		return view('Dashboard.index', compact('pending', 'active', 'students', 'staff', 'solve'));
	}
}