<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;



class StudentProfileController extends Controller
{
   public function index(){
   	return view('StudentProfile.index');
   }

   public function changePassword(){
   	return view('ChangePassword.index');
   }

   public function change(Request $request){
	$old_pass = $request->get('old_pass');
	$new_pass = $request->get('new_pass');
	$confirm_pass = $request->get('confirm_pass');

	$validator =  Validator::make($request->all(), [	
		'old_pass' => 'required',
		'new_pass' => 'required',
		'confirm_pass' => 'required'
	]);
	if ($validator->fails()) {
    	return response()->json(['status' => false, 'error' => $validator->errors()]);
	}else{
		$user = User::where('email' , Auth::user()->email)->first();
		if (Hash::check($old_pass, $user->password)) {
			if ($new_pass === $confirm_pass) {
			$user = User::where('user_id', Auth::user()->user_id)->first();
			$user->password = $new_pass;
			if ($user->save()) {
				return response()->json(['status' => true, 'message' => "Account updated successfully"]);
				
			}
			}else{
				return response()->json(['status' => false, 'message' => "New passwords do not match"]);
			}
			
		}else{
			return response()->json(['status' => false, 'message' => 'Old password do not match']);
		}
	}

   }

   public function upload(Request $request){
   	$user = User::where('user_id', Auth::user()->user_id)->first();
   	$user->user_image = $request->file('user_image')->store('user_img','public');
   	$user->save();
   	return response()->json(['status' => true, 'message' => 'Profile Image Uploaded Successfully']);

   }
}


		
    	