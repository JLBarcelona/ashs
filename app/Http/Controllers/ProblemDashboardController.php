<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Problem;
use App\Models\User;
use App\Models\Forum;
use Auth;



class ProblemDashboardController extends Controller
{
    public function index(){
    	return view('ProblemDashboard.index');
    }

    public function active(){
        return view('ProblemDashboard.active');
    }

    public function listActive(){
        $problem = Problem::with(['users'])->withCount('forums_is_unseen')->where('status', 1)->orderBy('created_at', 'desc')->get();
        return response()->json(['status' => true, 'data' => $problem]);
    }

    public function list(){
    	$problem = Problem::with('users')->where('status', '!=', 3)->where('status', '!=', 1)->orderBy('created_at', 'desc')->get();
    	return response()->json([
            'status' => true, 'data' => $problem]);
    }

    public function approve(Request $request){
       $problem_id  = $request->get('problem_id');
        $problem = Problem::where('id', $problem_id)->first();
        $problem->status = 1;
        $problem->save();
        return response()->json(['status' => true, 'message' => "Counseling request approve Successfully!"]); 
    }

    public function disapprove(Request $request){
       $problem_id  = $request->get('problem_id');
        $problem = Problem::where('id', $problem_id)->first();
        $problem->status = 2;
        $problem->save();
        return response()->json(['status' => true, 'message' => "Counseling request approve Successfully!"]); 
    }
    public function seen(Request $request){
    $problem = Problem::where('id', $request->get('id'))->first();
    $problem->admin_id = Auth::user()->user_id;
    $problem->seen_datetime = now();
    $problem->save();
    $forum = Forum::where('problem_id', $request->get('id'))->whereNull('sender_id')->get();
    foreach ($forum as $forum) {
        $forum->is_seen = 1;
        $forum->user_seener = Auth::user()->user_id;
        $forum->update();
    }
    return response()->json(['status' => true]);
    }

    public function solve(){
        return view('ProblemDashboard.solve');
    }

    public function listSolve(){
        $problem = Problem::with('users')->where('status', '=', 3)->orderBy('created_at', 'desc')->get();
        return response()->json(['status' => true, 'data' => $problem]);
    }
}
