<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Problem;
use App\Models\User;
use App\Models\Forum;
use Auth;

class InboxController extends Controller
{
    public function index(Request $request){
    	$problem = Problem::withCount('forums_is_notseen')->where('status','!=', '2')->where('status','!=', '0')->where('user_id', Auth::user()->user_id)->orderBy('status', 'asc')->orderBy('created_at', 'desc')->get();
    	return view('AccountInbox.index', compact('problem'));
    }

    public function seenUser($id){

    	$forum = Forum::where('problem_id', $id)->whereNull('receiver_id')->get();
	    foreach ($forum as $forum) {
            $forum->is_seen = 1;
            $forum->user_seener = Auth::user()->user_id;
	        $forum->update();
	    }

    	return response()->json(['status' => true, 'message' => 'sige']);
    }

}