<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Problem;
use App\Models\User;
use App\Models\Forum;
use Auth;


class ForumController extends Controller
{
    public function index(Request $request){
    	// dd($request->route('id'));
    	$problem = Problem::where('id', $request->route('id'))->first();
    	$prob = $problem->problem_type;
    	$probs = $problem->user_id;
    	$getUser = User::where('user_id',$probs )->first();
    	$user = $getUser->first_name.' '.$getUser->last_name;
    	$user_img = $getUser->user_image;
    	return view('Forum.index', compact('prob'), compact('user'), compact('user_img'));
    }

    public function send(Request $request){
		$url = str_replace(url('/'), '', url()->previous());
			if(preg_match("/\/(\d+)$/",$url,$matches)){
			  $end=$matches[1];
			}
    	 $forum = new Forum;
    	 $forum->problem_id = $end;
    	 $forum->message = $request->get('messages');
    	 $forum->sender_id = Auth::user()->user_id;
         $forum->is_seen = 0;
    	 $forum->save();
    	return response()->json(['status' => true, 'message' => 'message sent!' ]);
    }

    public function list(){
    	$url = str_replace(url('/'), '', url()->previous());
			if(preg_match("/\/(\d+)$/",$url,$matches)){
			  $end=$matches[1];
			}
    	$forum = Forum::where('problem_id', $end)->get();
    	return response()->json(['status' => true, 'data' => $forum]);
    }
}
