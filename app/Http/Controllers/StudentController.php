<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use Validator;

class StudentController extends Controller
{
	function index(){
		return view('Students.index');
	}


	function list(){
		$students = Student::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $students]);
	}


	function find($student_id){
		$students = Student::where('student_id', $student_id)->first();
		return response()->json(['status' => true, 'data' => $students]);
	}

	function add(Request $request){
		$student_id= $request->get('student_id');
		$firstname = $request->get('firstname');
		$middlename = $request->get('middlename');
		$lastname = $request->get('lastname');
		$birthdate = $request->get('birthdate');
		$gender = $request->get('gender');
		$address = $request->get('address');
		$grade = $request->get('grade');
		$section = $request->get('section');
		$password = $request->get('password');

		$validator = Validator::make($request->all(), [
			'firstname' => 'required',
			'middlename' => 'required',
			'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'address' => 'required',
			'grade' => 'required',
			'section' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($student_id)) {
				$students = Student::find($student_id);
				$students->firstname = $firstname;
				$students->middlename = $middlename;
				$students->lastname = $lastname;
				$students->birthdate = $birthdate;
				$students->gender = $gender;
				$students->address = $address;
				$students->grade = $grade;
				$students->section = $section;
				$students->email = $email;
				if($students->save()){
					return response()->json(['status' => true, 'message' => 'Students updated successfully!']);
				}
			}else{
				$students = new Student;
				$students->firstname = $firstname;
				$students->middlename = $middlename;
				$students->lastname = $lastname;
				$students->birthdate = $birthdate;
				$students->gender = $gender;
				$students->address = $address;
				$students->grade = $grade;
				$students->section = $section;
				$students->email = $email;
				$students->password = $password;
				if($students->save()){
					return response()->json(['status' => true, 'message' => 'Students saved successfully!']);
				}
			}
		}
	}


	function trash($student_id){
		$students = Student::find($student_id);
		if($students->delete()){
			return response()->json(['status' => true, 'message' => 'Students deleted successfully!']);
		}
	}

	
}