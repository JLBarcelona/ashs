<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mail;
use App\Mail\ForgetPassword;


class AuthController extends Controller
{
	public function index(){
			return view('Dashboard.index');
	}

	public function authenticate(Request $request){
		 $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	// $credentials = $request->validate([
	        //     'email' => ['required', 'email'],
	        //     'password' => ['required'],
	        // ]);

        	$credentials = ['email' => $request->get('email'), 'password' => $request->get('password'), 'user_status' => 1, 'deleted_at' => null];
	        if (Auth::attempt($credentials)) {
	            $request->session()->regenerate();
	            $user = Auth::user();
	            
	            if ($user->user_type == '3') {
	            	$redirect = route('problems.index');
	            }else{
	            	$redirect = route('admin.index');
	            }
	            return response()->json(['status' => true, 'redirect' => $redirect ]);
	        }else{
	        	$validator->errors()->add('password','Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	        }
        }
	}

	public function logout()
    {
		$user = Auth::user();
		Session::flush();
    	Auth::logout();
		return redirect('login');
		// if(save_auth_logs($user->id,'logout')) {
		// }
    }

    public function forgetPassword(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	$user = User::where('email', $request->get('email'))->count();
        	if ($user > 0) {
        	try {
        		$users = User::where('email', $request->get('email'))->first();
                 $data = [
                    'name' => $users->first_name,
                    'user_id' => $users->user_id,
                    'body' => '',
                 ];
                 Mail::to($request->get('email'))->send(new ForgetPassword($data));
                 return response()->json(['status' => true, 'message' => 'Email Sent! Please check your Gmail to recover your account']);
                 } catch (Exception $e) {
                 return response()->json(['status' => false, 'message' => 'Something went wrong!']);
        	}


        	}else{
        	return response()->json(['status' => false, 'message' => "Invalid email address!"]);

        	}

        }
    }

    public function resetPassword(Request $request){
         $validator = Validator::make($request->all(), [
            'new_pass' => 'required',
            'confirm_pass' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $url = str_replace(url('/'), '', url()->previous());
            if(preg_match("/\/(\d+)$/",$url,$matches)){
              $end=$matches[1];
            }
            if ($request->get('new_pass') == $request->get('confirm_pass')) {
                $users = User::where('user_id', $end)->first();
                $users->password = $request->get('new_pass');
                $users->save();
                return response()->json(['status' => true, 'message' => 'Password Successfully Change!']);
            }else{
                return response()->json(['status' => false, 'message' => "Password Does Not Match!"]);

            }
            

        }
    }
}