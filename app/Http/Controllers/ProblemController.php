<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Problem;
use Validator;
use Auth;

class ProblemController extends Controller
{
    public function index(){
        return view('Problem.index');
    }

    public function list(){
        $problems = Problem::where('user_id', Auth::user()->user_id)->orderBy('id', 'desc')->get();
        return response()->json(['status' => true, 'data' => $problems]);
    }

    public function save(Request $request, $id = ""){

        if($request->get('problem_type') == 'Other') {
            $validator = Validator::make($request->all(), [
            'problem_type' => 'required',
            'other_option' => 'required',
        ]);
        }else{
            $validator = Validator::make($request->all(), [
            'problem_type' => 'required',
        ]);
        }

        if($validator->fails()){
            return response()->json(['status' => false, 'error' => $validator->errors() ]);
        }else{
            if(!empty($id)){
            if (!empty ($request->get('other_option') )) {
                $problems = Problem::where('id', $id)->update([
                    'problem_type' => $request->get('other_option'),
                ]);
            }else{
                $problems = Problem::where('id', $id)->update([
                    'problem_type' => $request->get('problem_type'),
                ]);
                }
                if($problems){
                    return response()->json(['status' => true, 'message' => 'problems saved successfully!']);
                }
            }else{
                $problems = new Problem;
                $problems->user_id = Auth::user()->user_id;
                $problems->date = now();
            if (!empty ($request->get('other_option') )) {
                $problems->problem_type = $request->get('other_option');
                    
            }else{
                $problems->problem_type = $request->get('problem_type');
                
                }
                  $problems->save();
                  return response()->json(['status' => true, 'message' => 'problems saved successfully!']);


                }
                
            }
        }
    public function find($id){
        $problems = Problem::findOrFail($id);
        return response()->json(['status' => true, 'data' => $problems ]);
    }

    public function delete($id){
        $problems = Problem::findOrFail($id);
        if($problems->delete()){
            return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
        }
    }

}