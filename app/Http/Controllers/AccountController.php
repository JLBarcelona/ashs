<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Problem;
use App\Models\Forum;
use Validator;
use Auth;

class AccountController extends Controller
{
    public function index(){
        $inbox  = Problem::where('user_id', Auth::user()->user_id)->where('status', '!=', '2')->where('status', '!=', '0')->count();
        $pending = Problem::where('user_id', Auth::user()->user_id)->where('status', 0)->count();
        $active = Problem::where('user_id', Auth::user()->user_id)->where('status', 1)->count();
        $solve = Problem::where('user_id', Auth::user()->user_id)->where('status', 3)->count();
        $students = User::whereNull('deleted_at')->where('user_type', 3)->count();
        $staff = User::where('user_id', Auth::user()->user_id)->whereNull('deleted_at')->where('user_type', 2)->count();
        return view('AccountDashboard.index', compact('pending', 'active', 'students', 'staff', 'solve', 'inbox'));
    }

    public function find($id){
    	$users = User::where('user_Id', $id)->first();
    	return response()->json(['status' => true, 'data' => $users]);
    }

    public function edit(Request $request){
    	$first_name = $request->get('first_name');
        $middle_name = $request->get('middle_name');
        $last_name = $request->get('last_name');
        $gender = $request->get('gender');
        $contact = $request->get('contact');
        $province = $request->get('province');
        $city = $request->get('city');
        $address = $request->get('address');
        $email = $request->get('email');
        $password = $request->get('password');
        $student_no = $request->get('student_no');
        $strand = $request->get('strand');

      	$validator = Validator::make($request->all(),[
      		'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'section' => 'required',
            'province' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'student_no' => 'min:10|max:12|required',
            'strand' => 'required',
      	]);
      	if ($validator->fails()) {
      		return response()->json(['status' => false, 'error' => $validator->errors()]);
      	}else{
      		$users = User::where('user_id', Auth::user()->user_id)->first();
      			$users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->province = $province;
                $users->city = $city;
                $users->address = $address;
                $users->email = $email;
                $users->student_no = $student_no;
                $users->strand = $strand;
                $users->password = $password;
                $users->barangay = $request->get('barangay');
                $users->guardian_name = $request->get('guardian_name');
                $users->section = $request->get('section');
                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users updated successfully!']);
                }
      	}
    }
}
