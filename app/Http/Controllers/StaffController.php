<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Problem;
use App\Models\Forum;
use Validator;

class StaffController extends Controller
{
	function index(){
		return view('Staff.index');
	}

	function list(){
		$user_accounts = User::whereNull('deleted_at')->where('user_type', 2)->get();
		return response()->json(['status' => true, 'data' => $user_accounts]);
	}

	function find($user_id){
		$user_accounts = User::where('user_id', $user_id)->first();
		return response()->json(['status' => true, 'data' => $user_accounts]);
	}

	function add(Request $request){
		 $user_id= $request->get('user_id');
        $first_name = $request->get('first_name');
        $middle_name = $request->get('middle_name');
        $last_name = $request->get('last_name');
        $gender = $request->get('gender');
        $contact = $request->get('contact');
        $province = $request->get('province');
        $city = $request->get('city');
        $address = $request->get('address');
        $email = $request->get('email');
        $password = $request->get('password');
        

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'contact' => 'required',
            'province' => 'required',
            'city' => 'required',
            'address' => 'required',
            'email' => 'required ',
            
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            if (!empty($user_id)) {
                $users = User::find($user_id);
                $users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->province = $province;
                $users->city = $city;
                $users->address = $address;
                $users->email = $email;
                if (!empty($password)) {
                $users->password = $password;
                }
                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users updated successfully!']);
                }
            }else{
                $users = new User;
                $users->user_type = 2;
                $users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->province = $province;
                $users->city = $city;
                $users->address = $address;
                $users->email = $email;
                $users->password = $password;
                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users saved successfully!']);
                }
            }
        }
    }


	function trash($user_id){
		$user = User::find($user_id);
		$user->deleted_at = now();
        if ($user->save()) {
            return response()->json(['status' => true, 'message' => 'User_accounts deleted successfully!']);
           
        }
		
	}
}