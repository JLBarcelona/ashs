<?php 
 namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\NewsAndUpdate;
use Validator;

class UserController extends Controller
{
    function index(){
        return view('Users.index');
    }


    function list(){
        $users = User::whereNull('deleted_at')->get();
        return response()->json(['status' => true, 'data' => $users]);
    }


    function find($user_id){
        $users = User::where('user_id', $user_id)->first();
        return response()->json(['status' => true, 'data' => $users]);
    }

    function add(Request $request){
        $user_id= $request->get('user_id');
        $first_name = $request->get('first_name');
        $middle_name = $request->get('middle_name');
        $last_name = $request->get('last_name');
        $gender = $request->get('gender');
        $grade = $request->get('grade');
        $contact = $request->get('contact');
        $province = $request->get('province');
        $city = $request->get('city');
        $address = $request->get('address');
        $email = $request->get('email');
        $password = $request->get('password');
        $student_no = $request->get('student_no');
        $strand = $request->get('strand');

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'section' => 'required',
            'grade' => 'required',
            'province' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'min:6|required',
            'student_no' => 'min:10|max:12|required',
            'strand' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            if (!empty($user_id)) {
                $users = User::find($user_id);
                $users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->grade = $grade;
                $users->province = $province;
                $users->city = $city;
                $users->address = $address;
                $users->email = $email;
                $users->password = $password;
                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users updated successfully!']);
                }
            }else{
                $users = new User;
                $users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->province = $province;
                $users->city = $city;
                $users->grade = $grade;
                $users->address = $address;
                $users->email = $email;
                $users->student_no = $student_no;
                $users->strand = $strand;
                $users->password = $password;
                $users->barangay = $request->get('barangay');
                $users->guardian_name = $request->get('guardian_name');
                $users->section = $request->get('section');
                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users saved successfully!']);
                }
            }
        }
    }
    function trash($user_id){
        $users = User::find($user_id);
        if($users->delete()){
            return response()->json(['status' => true, 'message' => 'Users deleted successfully!']);
        }
    }

    public function landing(){
        $newsAndUpdate = NewsAndUpdate::get();
        return view('Landing.index', compact('newsAndUpdate'));
    }
}