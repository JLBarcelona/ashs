<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Problem;
use App\Models\Forum;
use Validator;
use Mail;
use App\Mail\EmailSend;

class UserAccountController extends Controller
{
	function index(){
       
		return view('User_accounts.index');
	}

	function list(Request $request){
       $filter_grade_section =  $request->get('filter_grade_section');
       $filter_strand = $request->get('filter_strand');

        if (!empty($filter_grade_section)) {
            $user_accounts = User::where('section','LIKE','%'.$filter_grade_section.'%')->whereNull('deleted_at')->where('user_type', 3)->orderBy('user_status', 'asc')->orderBy('user_id', 'desc')->get();

        }elseif(!empty($filter_strand)){
            $user_accounts = User::where('strand','LIKE','%'.$filter_strand.'%')->whereNull('deleted_at')->where('user_type', 3)->orderBy('user_status', 'asc')->orderBy('user_id', 'desc')->get();

        }elseif(!empty($filter_grade_section) || !empty($filter_strand)){
            $user_accounts = User::where('strand','=',$filter_strand)->where('section','LIKE','%'.$filter_grade_section.'%')->whereNull('deleted_at')->where('user_type', 3)->orderBy('user_status', 'asc')->orderBy('user_id', 'desc')->get();
        }else{
            $user_accounts = User::whereNull('deleted_at')->where('user_type', 3)->orderBy('user_status', 'asc')->orderBy('user_id', 'desc')->get();
        }
		return response()->json(['status' => true, 'data' => $user_accounts]);
	}

	function find($user_id){
		$user_accounts = User::where('user_id', $user_id)->first();
		return response()->json(['status' => true, 'data' => $user_accounts]);
	}

	function add(Request $request){
		$user_id= $request->get('user_id');
        $first_name = $request->get('first_name');
        $middle_name = $request->get('middle_name');
        $last_name = $request->get('last_name');
        $gender = $request->get('gender');
        $contact = $request->get('contact');
        $province = $request->get('province');
        $city = $request->get('city');
        $address = $request->get('address');
        $email = $request->get('email');
        $password = $request->get('password');
        $student_no = $request->get('student_no');
        $strand = $request->get('strand');


        if (empty($user_id)) {
            $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'section' => 'required',
            'grade' => 'required',
            'province' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'min:6|required',
            'student_no' => 'min:10|max:12|required',
            'strand' => 'required',
        ]);
        }else{
            $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'section' => 'required',
            'province' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'grade' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'student_no' => 'min:10|max:12|required',
            'strand' => 'required',
        ]);
        }
        

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            if (!empty($user_id)) {
                $users = User::find($user_id);
                $users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->province = $province;
                $users->city = $city;
                $users->grade = $request->get('grade');
                $users->address = $address;
                $users->email = $email;
                $users->student_no = $student_no;
                $users->strand = $strand;
                $users->barangay = $request->get('barangay');
                $users->guardian_name = $request->get('guardian_name');
                $users->section = $request->get('section');
                if (!empty($password)) {
                $users->password = $password;
                }else{
                    
                }

                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users updated successfully!']);
                }
            }else{
                $users = new User;
                $users->first_name = $first_name;
                $users->middle_name = $middle_name;
                $users->last_name = $last_name;
                $users->gender = $gender;
                $users->contact = $contact;
                $users->province = $province;
                $users->city = $city;
                $users->address = $address;
                $users->email = $email;
                $users->grade = $request->get('grade');
                $users->student_no = $student_no;
                $users->strand = $strand;
                $users->password = $password;
                $users->barangay = $request->get('barangay');
                $users->guardian_name = $request->get('guardian_name');
                $users->section = $request->get('section');
                if($users->save()){
                    return response()->json(['status' => true, 'message' => 'Users saved successfully!']);
                }
            }
        }
    }


	function trash($user_id){
		$user_accounts = User::find($user_id);
		$user_accounts->deleted_at = now();
        if ($user_accounts->save()) {
            return response()->json(['status' => true, 'message' => 'User_accounts deleted successfully!']);
           
        }
		}
	

    public function approve(Request $request){
        $user = User::where('user_id', $request->get('user_id'))->first();
        $user->user_status = '1';
        $user->save();


try {
                 $user = User::where('user_id', $request->get('user_id'))->first();
                 $data = [
                    'name' => $user->first_name,
                    'body' => '',
                 ];
                 Mail::to($user->email)->send(new EmailSend($data));
                 return response()->json(['status' => true, 'message' => 'Email Sent to this student!']);
                 } catch (Exception $e) {
                 return response()->json(['status' => false, 'message' => 'Something went wrong!']);
            
        }





    }

    public function disapprove(Request $request){
        $user = User::where('user_id', $request->get('user_id'))->first();
        $user->user_status = '2';
        $user->save();
        return response()->json(['status' => true, 'message' => 'Student account activated successfully!']);
    }
}