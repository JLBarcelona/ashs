<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Problem;
use App\Models\User;
use App\Models\Forum;
use Auth;

class AccountForumController extends Controller
{
    public function index(Request $request){
        
        $forumTable = Forum::where('problem_id', $request->route('id'))->first();
        $problem = Problem::where('id', $request->route('id'))->first();
        $status = $problem->status;
    	return view('AccountForum.index', compact('status'));
    }

    public function send(Request $request){
		$url = str_replace(url('/'), '', url()->previous());
			if(preg_match("/\/(\d+)$/",$url,$matches)){
			  $end=$matches[1];
			}
    	$forum = new Forum;
    	 $forum->problem_id = $end;
    	 $forum->message = $request->get('messages');
    	 $forum->receiver_id = Auth::user()->user_id;
         $forum->is_seen = 0;
    	 $forum->save();
    	return response()->json(['status' => true, 'message' => 'message sent!' ]);
    }

    public function list(){
    	$url = str_replace(url('/'), '', url()->previous());
			if(preg_match("/\/(\d+)$/",$url,$matches)){
			  $end=$matches[1];
			}
    	$forum = Forum::where('problem_id', $end)->get();
    	return response()->json(['status' => true, 'data' => $forum]);
    }
    public function solve(Request $request){
        $url = str_replace(url('/'), '', url()->previous());
            if(preg_match("/\/(\d+)$/",$url,$matches)){
              $end=$matches[1];
            }
       $problem_id = Problem::where('id', $end)->first();
       $problem_id->status = 3;
       $problem_id->save();
       return response()->json(['status' => true, 'message' => "Problem Solve!"]);
    }
}
