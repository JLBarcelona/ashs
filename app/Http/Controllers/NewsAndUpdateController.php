<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NewsAndUpdate;


class NewsAndUpdateController extends Controller
{
    public function index(){
       $news = NewsAndUpdate::get();
        return view('NewsAndUpdate.index', compact('news'));

    }
 
    public function activities(Request $request){
       $activities =  $request->get('this');
       $newsAndUpdate = NewsAndUpdate::where('id', '1')->first();
       $newsAndUpdate->activities = $activities;
       if ($newsAndUpdate->save()) {
         return response()->json(['status' => true, 'message' => 'success']);

       }

    }

    public function news(Request $request){
       $news =  $request->get('this');
       $newsAndUpdate = NewsAndUpdate::where('id', '1')->first();
       $newsAndUpdate->news = $news;
       if ($newsAndUpdate->save()) {
         return response()->json(['status' => true, 'message' => 'success']);

       }

    }
}
