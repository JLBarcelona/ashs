<?php 

use App\Models\NewsAndUpdate;
use App\Models\User;
use App\Models\Problem;
use App\Models\Forum;


function notif_problem(){
	return Problem::where('status', 0)->count();
}

function unread_message(){
	return Forum::whereNull('sender_id')->where('is_seen', 0)->count();
}

function new_students(){
	return User::where('user_type', 3)->where('user_status', 0)->whereNull('deleted_at')->count();
}

function count_all_notifs(){
	$problem =  Problem::where('status', 0)->count();
	$forum =  Forum::whereNull('sender_id')->where('is_seen', 0)->count();
	$user =  User::where('user_type', 3)->where('user_status', 0)->whereNull('deleted_at')->count();
	return $total = $problem + $forum + $user;
}

function get_student_gender(){
	$male = User::where('user_type', 3)->where('gender', 'Male')->count();
	$female = User::where('user_type', 3)->where('gender', 'Female')->count();
	return ['male' => $male, 'female' => $female];
}

function get_student_per_section(){
	$sections = User::select('section')->where('user_type', 3)->whereNotNull('section')->distinct()->get();
	$data = [];
	$data_girls =  [];
	$data_boys =  [];
	$label = [];
	foreach ($sections as $section) {
		$label[] = $section->section;
		$data_boys[] = User::where('section', $section->section)->where('gender', 'Male')->count();
		$data_girls[] = User::where('section', $section->section)->where('gender', 'Female')->count();
	}

	return ['label' => $label, 'data_boys' => $data_boys, 'data_girls' => $data_girls];
}

function get_student_grades(){
	$twelve = User::where('user_type', 3)->where('grade', 'Grade 12')->count();
	$eleven = User::where('user_type', 3)->where('grade', 'Grade 11')->count();
	return ['eleven' => $eleven, 'twelve' => $twelve];
}

function get_student_strands(){
	$TVL = User::where('user_type', 3)->where('strand', 'TVL')->count();
	$ABM = User::where('user_type', 3)->where('strand', 'ABM')->count();
	$HUMSS = User::where('user_type', 3)->where('strand', 'HUMSS')->count();
	$STEM = User::where('user_type', 3)->where('strand', 'STEM')->count();
	return [
		'TVL' => $TVL,
	 	'ABM' => $ABM,
	 	'HUMSS' => $HUMSS,
	 	'STEM' => $STEM,
	];
}

function news(){
	 $news = NewsAndUpdate::first();
	 return $news->news;
}

function activities(){
	 $activities = NewsAndUpdate::first();
	 return $activities->activities;
}

// function count_unseen_msg_admin(){
// 	return Forum::where('receiver_id', Auth::user()->user_id)->whereNull('user_seener')->count();	
// }

