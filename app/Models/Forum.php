<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Forum extends Model
{
    use HasFactory;

    protected $table = 'forums';

    protected $fillable = [
        'problem_id',
        'receiver_id',
        'sender_id',
        'message',
        'is_seen',
        'user_seener',
    ];

    // public function getCreatedAtAttribute($value){
    //     $date = Carbon\Carbon::parse($value)->format('Y-m-d H:i:s');
    //     return $date->diffForHumans();
    // }

    public function problems(){
        return $this->belongsTo('App\Models\Problem', 'problem_id' , 'id');
    }

    public function count_unseen_msg_admin(){
    return Forum::where('receiver_id', Auth::user()->user_id)->whereNull('user_seener')->count();   
}

}
