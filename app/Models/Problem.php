<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
    use HasFactory;

    protected $fillable = [
        'problem_type',
        'user_id',
        'problem_message',
        'date',
    ];

    public function users(){
        return $this->belongsTo('App\Models\User', 'user_id' , 'user_id');
    }

    public function forums_is_unseen(){
        return $this->hasMany('App\Models\Forum', 'problem_id' , 'id')->whereNull('sender_id')->where('is_seen', 0);
    }

    public function forums_is_notseen(){
        return $this->hasMany('App\Models\Forum', 'problem_id' , 'id')->whereNull('receiver_id')->where('is_seen', 0);
    }
    
}